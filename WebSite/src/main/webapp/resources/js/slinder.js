
$(document).ready(function() {
    $('a.add-btn').click(function() {
        // Getting the variable's value from a link 
        var loginBox = $(this).attr('href');
        //Fade in the Popup and add close button
        $(loginBox).fadeIn(300);
        //Set the center alignment padding + border
        var popMargTop = ($(loginBox).height() + 24) / 2;
        var popMargLeft = ($(loginBox).width() + 24) / 2;
        $(loginBox).css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });
        // Add the mask to body
        $('body').append('<div id="mask"></div>');
        $('#mask').fadeIn(300);
        return false;
    });
});

$(document).ready(function() {
    $('.slinder-dialog').hide();
    $('a.close').on('click', function(e) {
        e.preventDefault();
        var elem = $(this).next('.login-popup2');
        $('.login-popup2').hide();
        $('#mask').hide();
        elem.toggle('slow');
    });
});
