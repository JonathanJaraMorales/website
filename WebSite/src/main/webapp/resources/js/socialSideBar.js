$(document).ready(function() {
    $('.toggle').hide();
    $('a.togglelink').on('click', function(e) {
        e.preventDefault();
        var elem = $(this).next('.toggle');
        $('.toggle').not(elem).hide('slow');
        $('.toggle').animate({'width': '280px'}, 100);
        $('.li-addons').animate({'margin-left': '-310px'}, 100);
        if ($(elem).is(":visible")) {
            $('.li-addons').animate({'margin-left': '0px'}, 100);
        }
        elem.toggle('slow');
        $("#youmax-uploads").click();
        $("#youmax-featured").click();
    });
});