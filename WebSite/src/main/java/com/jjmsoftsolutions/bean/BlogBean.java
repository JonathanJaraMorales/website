package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.domain.Email;
import com.jjmsoftsolutions.entity.Blog;
import com.jjmsoftsolutions.entity.BlogProperties;
import com.jjmsoftsolutions.entity.BlogSecurity;
import com.jjmsoftsolutions.model.BlogModel;
import com.jjmsoftsolutions.utils.Constants;
import com.jjmsoftsolutions.utils.FtpUtil;
import com.jjmsoftsolutions.utils.JSFUtil;
import com.jjmsoftsolutions.utils.Session;
import com.jjmsoftsolutions.utils.StringUtils;
import com.jjmsoftsolutions.utils.Utility;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.Part;

@ManagedBean(name = "BlogBean")
@ViewScoped
public class BlogBean implements Serializable {
    
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private Blog blog = new Blog();
    private boolean active;
    private String originalPassword;
    private Part file;

    private String errorMessage;
    private String sucessMessage;

    private String oldPassword;
    private String newPassword;
    private String revalidatePassword;
    private Blog session;

    private BlogProperties properties = new BlogProperties();
    private BlogSecurity security = new BlogSecurity();

    private boolean sendEmailSermon;
    private boolean sendEmailEvent;
    private boolean sendEmailCuriosity;
    private boolean sendEmailPhotos;
    private boolean sendEmailPost;

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public boolean getActivate() {
        String cd_pw = JSFUtil.getParameterURL("cd_pw");
        String em = JSFUtil.getParameterURL("em");
        List<Blog> list = BlogModel.getBlogByPassword(cd_pw);
        for (Blog b : list) {
            String emailEncrypt = StringUtils.md5(b.getEmail());
            if (em.equals(emailEncrypt)) {
                b.setActivate('Y');
                BlogModel.update(b);
            }
        }
        return false;
    }

    public void delete() {
        boolean result = BlogModel.delete(Session.getUser());
        Session.logout();
        Utility.redirect("Inicio");

    }

    public void insert() {

        if (validateFields()) {
            boolean exist = validateAccount();

            if (exist) {

                setPhoto();

                originalPassword = blog.getPassword();
                blog.setPassword(StringUtils.md5(blog.getPassword()));
                blog.setActivate('F');
                blog.setLoginDate(new Date());
                blog.setLastLoginDate(new Date());

                try {
                    blog.setLastIpAccess(Session.getIp());
                } catch (Exception ex) {
                    Logger.getLogger(BlogBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                BlogModel.insert(blog);
                sendInsertEmail();
                sucessMessage = "Cuenta Ingresada correctamente, favor verifique su correo y active su cuenta. "
                        + "Su configuración de seguridad esta definida, para que solo usted pueda ver su información, "
                        + "si desea cambiarlo, accese a su cuenta y en la pestaña de Seguridad puede modificar esta configuración";

            } else {
                errorMessage = "La cuenta ya existe";
            }

        }

    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the sucessMessage
     */
    public String getSucessMessage() {
        return sucessMessage;
    }

    /**
     * @param sucessMessage the sucessMessage to set
     */
    public void setSucessMessage(String sucessMessage) {
        this.sucessMessage = sucessMessage;
    }

    private boolean validateAccount() {
        return BlogModel.getBlogByEmail(blog.getEmail()) == null;
    }

    private void sendInsertEmail() {
        StringBuilder msg = new StringBuilder();
        msg.append("<html> ");
        msg.append("<body  style=\"font-family: 'Lucida Grande', 'Lucida Sans Unicode', sans-serif\"> ");
        msg.append("<div style=\"color: #20A53B; font-size: 20px\"> ");
        msg.append("Verificación Email ");
        msg.append("</div> ");
        msg.append("<div style=\"font-size: 14px\"> ");
        msg.append("<p> ");
        msg.append("¡Hola! ").append(blog.getUsername()).append(", ");
        msg.append("<br/><br/> ");
        msg.append("Gracias por unirse a nuestra comunidad, a continuación encontrará su nombre de usuario y contraseña, para poder hacer ingreso a la página; ");
        msg.append("favor presione el boton activar para poder accesar sin ningun problema ");
        msg.append("<br/><br/> ");
        msg.append("<strong>Usuario</strong>:").append(blog.getUsername()).append("<br/> ");
        msg.append("<strong>Contraseña</strong>:").append(originalPassword).append("<br/> ");
        msg.append("</p> ");
        msg.append("</div> ");
        msg.append("<div style=\"width: 58px; height: 17px; filter: Alpha(Opacity=50); opacity: 0.5;  -webkit-border-radius: 20px; -moz-border-radius: 20px;    background: #20A53B; border: none; color: #FFFFFF; cursor: pointer  \"> ");
        msg.append("<a href=\"");
        msg.append("http://www.jjmsoftsolutions.com/ActivarCuenta?em=").append(StringUtils.md5(blog.getEmail())).append("&cd_pw=").append(blog.getPassword());
        msg.append("\" target=\"_blank\" style=\"text-decoration: none;   font-size: 14px;  margin-left:5px; color:#FFFFFF \" >Activar</a> ");
        msg.append("</div><br/> ");
        msg.append("<div style=\" font-size: 12px\"> ");
        msg.append("2014 CCI Vida Abundante +Q Joven. All Rights Reserved ");
        msg.append("</div> ");
        msg.append("</body>");
        msg.append("</html>");
        Email email = new Email();
        email.send(blog.getEmail(), "Verificación Email", msg.toString(), "");
    }

    private void sendUpdatePasswordEmail() {
        StringBuilder msg = new StringBuilder();
        msg.append("<html> ");
        msg.append("<body  style=\"font-family: 'Lucida Grande', 'Lucida Sans Unicode', sans-serif\"> ");
        msg.append("<div style=\"color: #20A53B; font-size: 20px\"> ");
        msg.append("Actualización de contraseña");
        msg.append("</div> ");
        msg.append("<div style=\"font-size: 14px\"> ");
        msg.append("<p> ");
        msg.append("¡Hola! ").append(blog.getUsername()).append(", ");
        msg.append("<br/><br/> ");
        msg.append("Su nueva contraseña ha sido actualizada. ");
        msg.append("<br/><br/> ");
        msg.append("<strong>Usuario</strong>:").append(blog.getUsername()).append("<br/> ");
        msg.append("<strong>Contraseña</strong>:").append(originalPassword).append("<br/> ");
        msg.append("</p> ");
        msg.append("</div> ");
        msg.append("<div style=\" font-size: 12px\"> ");
        msg.append("2014 CCI Vida Abundante +Q Joven. All Rights Reserved ");
        msg.append("</div> ");
        msg.append("</body>");
        msg.append("</html>");
        Email email = new Email();
        email.send(blog.getEmail(), "Actualización de Contraseña", msg.toString(), "");
    }

    public Blog getBlogSession() {
        return Session.getUser();
    }

    public Blog getBlogByEmail() {
        String email = JSFUtil.getParameterURL("id");
        setBlog(BlogModel.getBlogByEmail(email));
        return getBlog();
    }

    public Blog getBlogByEmailEdit() {

        String urlEmail = JSFUtil.getParameterURL("id");

        Blog session = Session.getUser();

        if (session != null && urlEmail == null) {
            String email = JSFUtil.getParameterURL("id");
            setBlog(BlogModel.getBlogByEmail(email));
            return getBlog();
        } else if (session != null && urlEmail.equals(session.getEmail())) {
            String email = JSFUtil.getParameterURL("id");
            setBlog(BlogModel.getBlogByEmail(email));
            return getBlog();
        } else {
            Utility.redirect("Ver-Perfil?id=" + urlEmail + "&post=3&sermon=3");
            return null;
        }

    }

    public void updateSecurity() {
        sucessMessage = "";
        BlogModel.updateSecurity(session.getSecurity());
        sucessMessage = "Cambiada la configuración de seguridad";
    }

    public void updatePersonalInformation() {
        BlogModel.update(session);
        sucessMessage = "Actualizado los datos personales";
    }

    public void changePhoto() {
        setPhoto();
        Utility.redirect("Perfil?id=" + blog.getEmail() + "&post=3&sermon=3");
    }

    /**
     * @return the oldPassword
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * @param oldPassword the oldPassword to set
     */
    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the revalidatePassword
     */
    public String getRevalidatePassword() {
        return revalidatePassword;
    }

    /**
     * @param revalidatePassword the revalidatePassword to set
     */
    public void setRevalidatePassword(String revalidatePassword) {
        this.revalidatePassword = revalidatePassword;
    }

    public void changePassword() {
        originalPassword = revalidatePassword;
        String encryptOldPassword = StringUtils.md5(oldPassword);
        String encryptNewPassword = StringUtils.md5(newPassword);
        String encryptRevalidatePassword = StringUtils.md5(revalidatePassword);

        if (encryptNewPassword.equals(encryptRevalidatePassword)) {
            if (encryptOldPassword.equals(blog.getPassword())) {
                blog.setPassword(encryptNewPassword);
                BlogModel.updatePassword(blog);
                sendUpdatePasswordEmail();
            } else {
                errorMessage = "La contraseña antigua no coincide con su actual contraseña";
            }
        } else {
            errorMessage = "Las contraseñas tienen que ser iguales";
        }
    }

    private void setPhoto() {
        if (file != null) {
            try {
                int index = file.getContentType().lastIndexOf("/");
                String type = file.getContentType().substring(index + 1);
                FtpUtil.upload("ProfilePictures", file.getInputStream(), blog.getEmail().concat(".").concat(type));
                blog.setPicture(Constants.PROFILE_PICTURE.concat(blog.getEmail()).concat(".").concat(type));
            } catch (IOException ex) {
                Logger.getLogger(BlogBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @return the session
     */
    public Blog getSession() {
        if (session == null) {
            setSession(Session.getUser());
        }
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(Blog session) {
        this.session = session;
    }

    public void getResetMsg(AjaxBehaviorEvent e) {
        sucessMessage = "";
        errorMessage = "";
    }

    /**
     * @return the properties
     */
    public BlogProperties getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(BlogProperties properties) {
        this.properties = properties;
    }

    /**
     * @return the security
     */
    public BlogSecurity getSecurity() {
        return security;
    }

    /**
     * @param security the security to set
     */
    public void setSecurity(BlogSecurity security) {
        this.security = security;
    }

    /**
     * @return the sendEmailSermon
     */
    public boolean getSendEmailSermon() {
        return sendEmailSermon;
    }

    /**
     * @param sendEmailSermon the sendEmailSermon to set
     */
    public void setSendEmailSermon(boolean sendEmailSermon) {
        this.sendEmailSermon = sendEmailSermon;
    }

    /**
     * @return the sendEmailEvent
     */
    public boolean getSendEmailEvent() {
        return sendEmailEvent;
    }

    /**
     * @param sendEmailEvent the sendEmailEvent to set
     */
    public void setSendEmailEvent(boolean sendEmailEvent) {
        this.sendEmailEvent = sendEmailEvent;
    }

    /**
     * @return the sendEmailCuriosity
     */
    public boolean getSendEmailCuriosity() {
        return sendEmailCuriosity;
    }

    /**
     * @param sendEmailCuriosity the sendEmailCuriosity to set
     */
    public void setSendEmailCuriosity(boolean sendEmailCuriosity) {
        this.sendEmailCuriosity = sendEmailCuriosity;
    }

    /**
     * @return the sendEmailPhotos
     */
    public boolean getSendEmailPhotos() {
        return sendEmailPhotos;
    }

    /**
     * @param sendEmailPhotos the sendEmailPhotos to set
     */
    public void setSendEmailPhotos(boolean sendEmailPhotos) {
        this.sendEmailPhotos = sendEmailPhotos;
    }

    /**
     * @return the sendEmailPost
     */
    public boolean getSendEmailPost() {
        return sendEmailPost;
    }

    /**
     * @param sendEmailPost the sendEmailPost to set
     */
    public void setSendEmailPost(boolean sendEmailPost) {
        this.sendEmailPost = sendEmailPost;
    }

    private boolean validateFields() {
        if(blog.getName().trim().length() == 0){
            errorMessage = "El nombre es requerido";
            return false;
        }
        
        if(blog.getName().trim().length() > 45){
            errorMessage = "El nombre tiene que ser menor a 45 caractéres";
            return false;
        }
        
        if(blog.getFirstName().trim().length() == 0){
            errorMessage = "El primer apellido es requerido";
            return false;
        }
        
         if(blog.getFirstName().trim().length() > 45){
            errorMessage = "El primer apellido tiene que ser menor a 45 caractéres";
            return false;
        }
        
        if(blog.getLastName().trim().length() == 0){
            errorMessage = "El segundo apellido es requerido";
            return false;
        }
        
         if(blog.getLastName().trim().length() > 45){
            errorMessage = "El segundo apellido tiene que ser menor a 45 caractéres";
            return false;
        }
        
        if(blog.getEmail().trim().length() == 0){
            errorMessage = "El correo electrónico es requerido";
            return false;
        }
        
        if(blog.getEmail().trim().length() > 50){
            errorMessage = "El correo electrónico debe ser menor a 50 caractéres";
            return false;
        }
        
        if(validateEmail(blog.getEmail())){
            errorMessage = "El correo electrónico es invalido. El correo deberia de ser similar a ccividaabundate@gmail.com";
            return false;
        }
        
        if(blog.getGender() == ' '){
            errorMessage = "El género es requerido";
            return false;
        }
        
        if(blog.getUsername().trim().length()==0){
            errorMessage = "El nombre de usuario es requerido";
            return false;
        }
        
        if(blog.getUsername().trim().length()>15){
            errorMessage = "El nombre de usuario debe ser menor a 15 caractéres";
            return false;
        }
        
        if(blog.getPassword().trim().length()==0){
            errorMessage = "La Contraseña es requerida";
            return false;
        }
        
        return true;   
    
    }
    
    public static boolean validateEmail(String email) { 
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches(); 
    }

}
