package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.domain.Tag;
import com.jjmsoftsolutions.entity.Post;
import com.jjmsoftsolutions.entity.PostComment;
import com.jjmsoftsolutions.model.PostModel;
import com.jjmsoftsolutions.model.VideoModel;
import com.jjmsoftsolutions.utils.JSFUtil;
import com.jjmsoftsolutions.utils.Session;
import com.jjmsoftsolutions.utils.StringUtils;
import com.jjmsoftsolutions.utils.Utility;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "PostBean")
@ViewScoped
public class PostBean implements Serializable {

    private Post post;
    private List<Post> popular;
    private List<Post> news;

    private PostComment comment = new PostComment();

    public Post getLastPost() {
        setPost(PostModel.getLastPost("Reflexion"));
        return getPost();
    }

    public List<Post> getEvents() {
        String category = "Eventos";
        int end = StringUtils.getInt(JSFUtil.getParameterURL("end"));
        return PostModel.getAllPostByCategory(category, end);
    }

    public Post getPostById() {
        setPost(PostModel.getPostById(StringUtils.getInt(JSFUtil.getParameterURL("id"))));
        return getPost();
    }

    public Post getEventById() {
        setPost(PostModel.getPostByIdCategory(StringUtils.getInt(JSFUtil.getParameterURL("id")), "Eventos"));
        return getPost();
    }

    public List<Post> getPopularPost() {
        setPopular(PostModel.getPopularPost(8));
        return getPopular();
    }

    public List<Post> getCuriosities() {
        return PostModel.getPostByCategory("Curiosidades", 10);
    }

    public List<Post> getNewsIndex() {
        setNews(PostModel.getPostByCategory("Eventos", 9));
        return getNews();
    }

    /**
     * @return the post
     */
    public Post getPost() {
        return post;
    }

    /**
     * @param post the post to set
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * @return the popular
     */
    public List<Post> getPopular() {
        return popular;
    }

    /**
     * @param popular the popular to set
     */
    public void setPopular(List<Post> popular) {
        this.popular = popular;
    }

    /**
     * @return the news
     */
    public List<Post> getNews() {
        return news;
    }

    /**
     * @param news the news to set
     */
    public void setNews(List<Post> news) {
        this.news = news;
    }

    public void postComment() {
        getComment().setCommentDate(new Date());
        getComment().setEmail(Session.getUser());
        getComment().setIdPost(post);
        PostModel.postComment(getComment());
        Utility.redirect("Post?id=" + post.getIdPost());
    }

    /**
     * @return the comment
     */
    public PostComment getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(PostComment comment) {
        this.comment = comment;
    }

    public void deletePostComment(int id) {
        PostModel.delete(id);
        Utility.redirect("post.xhtml?id=" + post.getIdPost());
    }

    public List<Tag> getTotalButtons() {

        int pagination = StringUtils.getInt(JSFUtil.getParameterURL("end"));
        int totalRecord = PostModel.getCountByCategory("Eventos");
        return Utility.getTag(3, totalRecord, pagination);
    }

    public List<PostComment> getCommentsByBlog() {
        String email = JSFUtil.getParameterURL("id");
        int totalRecords = StringUtils.getInt(JSFUtil.getParameterURL("post"));
        return PostModel.getAllPostByCommentsByBlog(email, totalRecords);
    }

    public List<Tag> getTotalCommentsPost() {
        int pagination = StringUtils.getInt(JSFUtil.getParameterURL("post"));
        int totalRecord = PostModel.getCountCommentsByBlog(JSFUtil.getParameterURL("id"));
        return Utility.getTag(3, totalRecord, pagination);

    }

}
