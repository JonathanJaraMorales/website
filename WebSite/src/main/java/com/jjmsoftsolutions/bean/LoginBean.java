package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.entity.Blog;
import com.jjmsoftsolutions.entity.BlogSession;
import com.jjmsoftsolutions.model.BlogModel;
import com.jjmsoftsolutions.model.LoginModel;
import com.jjmsoftsolutions.utils.JSFUtil;
import com.jjmsoftsolutions.utils.Session;
import static com.jjmsoftsolutions.utils.Session.getSession;
import com.jjmsoftsolutions.utils.StringUtils;
import com.jjmsoftsolutions.utils.Utility;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

@ManagedBean(name = "login")
@SessionScoped
public class LoginBean {
    
    private String name;
    private String password;
    private static HashMap<String, String> blogConnected;

    public LoginBean() {
        if (blogConnected == null) {
            blogConnected = new HashMap<String, String>();
        }
    }
    
     /**
     * @return the blogConnected
     */
    public static HashMap<String, String>  getBlogConnected() {
        return blogConnected;
    }

    /**
     * @param aBlogConnected the blogConnected to set
     */
    public static void setBlogConnected(HashMap<String, String> aBlogConnected) {
        blogConnected = aBlogConnected;
    }


    public void login() {
        Blog user = LoginModel.login(name, StringUtils.md5(password));
        if (user != null) {
            try {
                String ip = Session.getIp();

                BlogSession blogSession = new BlogSession();
                blogSession.setEmail(user);
                blogSession.setIp(ip);
                blogSession.setLoginDate(new Date());

                HttpSession session = getSession();
                session.setAttribute("user", user);
                user.setLastLoginDate(new Date());
                user.setLastIpAccess(ip);
                LoginModel.update(user);
                BlogModel.insertSession(blogSession);
                blogConnected.put(user.getEmail(), user.getEmail());
                Utility.redirect("Inicio");
                getBlogConnected().put(user.getEmail(), user.getEmail());
            } catch (Exception ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            user = LoginModel.isDisable(name, StringUtils.md5(password));
            if (user != null) {
                Utility.redirect("Cuenta-Deshabilitada");
            } else {
                Utility.redirect("Cuenta-Invalida");
            }
        }
    }

    public boolean getIsLogin() {
        return getUser() != null;
    }

    public Blog getUser() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        Blog user = (Blog) session.getAttribute("user");
        return user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void logout() {
        blogConnected.remove(Session.getUser().getEmail());
        Session.logout();
        Utility.redirect("Inicio");
        
    }

    public void profile() {
        Utility.redirect("Perfil?id=".concat(Session.getUser().getEmail()).concat("&sermon=3&post=3"));
    }
    
    public boolean getUserIsConnected(){
        String email = JSFUtil.getParameterURL("id");
        if(blogConnected != null){
            return blogConnected.containsKey(email);
        }
        return false;
        
    }

}
