package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.entity.Poll;
import com.jjmsoftsolutions.entity.PollOptions;
import com.jjmsoftsolutions.entity.PollStats;
import com.jjmsoftsolutions.model.PollModel;
import com.jjmsoftsolutions.utils.Session;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "PollBean")
@ViewScoped
public class PollBean implements Serializable{


    private Poll poll;

    public Poll getUltimatePoll() {
        setPoll(PollModel.getLastPoll());
        return getPoll();
    }

    /**
     * @return the poll
     */
    public Poll getPoll() {
        return poll;
    }

    /**
     * @param poll the poll to set
     */
    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    
    public void vote(Integer selectedOption) {
        PollStats stats = new PollStats();
        stats.setEmail(Session.getUser());
        stats.setIdPollOption(new PollOptions(selectedOption));
        stats.setIdPoll(poll);
        PollModel.setVote(stats);       
    }
    
    public void delete(Integer selectedOption) {   
        PollModel.delete(selectedOption);       
    }
    
     public void change(Integer delete, Integer insert) {
         delete(delete);
         vote(insert);
    }

}
