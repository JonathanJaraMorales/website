package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.utils.Constants;
import com.jjmsoftsolutions.utils.JSFUtil;
import com.jjmsoftsolutions.utils.Session;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;

@ManagedBean (name ="GeneralBean")
@NoneScoped
public class GeneralBean implements Serializable{
    
    public String getLogoImage(){
        return Constants.temporaryWebPageUrl +"public/img/logo/+Q.png";
    }
    
    public String getPictureBlog(){
        return Session.getUser().getPicture();
    }
    
    public String getPostParameter(){
        return JSFUtil.getParameterURL("post");
    }
    
    public String getSermonParameter(){
        return JSFUtil.getParameterURL("sermon");
    }
    
    public String getEmailParameter(){
        return JSFUtil.getParameterURL("id"); 
    }
    
   
}
