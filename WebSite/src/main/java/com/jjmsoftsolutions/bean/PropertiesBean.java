package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.entity.Properties;
import com.jjmsoftsolutions.model.PropertiesModel;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

@ManagedBean(name = "PropertiesBean")
@ViewScoped
public class PropertiesBean implements Serializable{

    private String errorMessage;
    private String sucessMessage;
    
    private Properties properties = new Properties();
    private int slinderPagination;

    public int getSlinderPagination() {
        properties  = PropertiesModel.getProperties();        
        return properties.getSlinderPagination();
    }
    
    public Properties getAllProperties(){
        return PropertiesModel.getProperties();
    }
    
    public void update(){
        PropertiesModel.update(properties);
        sucessMessage = "Actualizado la paginación de la sección slinder";
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the sucessMessage
     */
    public String getSucessMessage() {
        return sucessMessage;
    }

    /**
     * @param sucessMessage the sucessMessage to set
     */
    public void setSucessMessage(String sucessMessage) {
        this.sucessMessage = sucessMessage;
    }

    /**
     * @return the properties
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
    
    public void getChangeSlinderPagination(AjaxBehaviorEvent event){
       slinderPagination = properties.getSlinderPagination();
       System.err.println(slinderPagination);
    }

}
