package com.jjmsoftsolutions.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

@ManagedBean(name = "PageBean")
public class PageBean {

    public String getSelectedBean() {    
        return FacesContext.getCurrentInstance().getViewRoot().getViewId();
    }
}
