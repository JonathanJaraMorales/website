package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.entity.Picture;
import com.jjmsoftsolutions.entity.PictureAlbum;
import com.jjmsoftsolutions.model.PictureAlbumModel;
import com.jjmsoftsolutions.model.PictureModel;
import com.jjmsoftsolutions.model.PropertiesModel;
import com.jjmsoftsolutions.utils.Constants;
import com.jjmsoftsolutions.utils.FtpUtil;
import com.jjmsoftsolutions.utils.JSFUtil;
import com.jjmsoftsolutions.utils.StringUtils;
import com.jjmsoftsolutions.utils.Utility;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

@ManagedBean(name = "PictureAlbumBean")
@Stateless
public class PictureAlbumBean implements Serializable {

    private List<PictureAlbum> recentAlbums = null;
    private String sucessMessage;
    private String errorMessage;
    private Part file;

    public List<Picture> getPictureByAlbum() {
        return null;
    }

    public List<PictureAlbum> getAlbums() {
        List<PictureAlbum> list = PictureAlbumModel.getAllAlbums();
        return list;
    }

    public PictureAlbum getAlbum() {
        int id = StringUtils.getInt(JSFUtil.getParameterURL("id"));
        return PictureAlbumModel.getAlbumById(id);
    }

    public List<Picture> getPicturesByIdAlbum() {
        int id = StringUtils.getInt(JSFUtil.getParameterURL("id"));
        PictureAlbum album = PictureAlbumModel.getAlbumById(id);
        if (album != null) {
            return album.getPictureList();
        }
        return null;
    }

    public List<Picture> getCoreografiaMinisterio() {
        String category = "min-coreografia";
        PictureAlbum album = PictureAlbumModel.getAlbum(category);
        if (album != null) {
            return album.getPictureList();
        }
        return null;
    }

    public List<Picture> getPictureSlinder() {
        int pagination = PropertiesModel.getSlinderPagination();
        return PictureModel.getPicturesByAlbum("Slinder", pagination);
    }

    public List<Picture> getAllPictureSlinder() {
        return PictureModel.getPicturesByAlbum("Slinder");
    }

    public List<PictureAlbum> getRecentAlbums() {
        int max = 9;
        recentAlbums = PictureAlbumModel.getRecentAlbums(max);
        return recentAlbums;
    }

    /**
     * @param recentAlbums the recentAlbums to set
     */
    public void setRecentAlbums(List<PictureAlbum> recentAlbums) {
        this.recentAlbums = recentAlbums;
    }

    /**
     * @return the sucessMessage
     */
    public String getSucessMessage() {
        return sucessMessage;
    }

    /**
     * @param sucessMessage the sucessMessage to set
     */
    public void setSucessMessage(String sucessMessage) {
        this.sucessMessage = sucessMessage;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void insertSlinder() {
        if (file != null) {
            try {
                String fileName = Utility.getFileName(file);
                Picture picture = new Picture();
                picture.setPictureDate(new Date());
                picture.setIdPictureAlbum(PictureAlbumModel.getAlbum("Slinder"));
                picture.setTitle("");
                FtpUtil.upload("img/slinder", file.getInputStream(), fileName);
                picture.setUrl(Constants.SLINDER_PICTURE.concat(fileName));
                PictureModel.insert(picture);
                sucessMessage = "Ingresada correctamente";
                errorMessage = "";
            } catch (IOException ex) {
                Logger.getLogger(PictureAlbumBean.class.getName()).log(Level.SEVERE, null, ex);
                errorMessage = "Error al ingresar la imágen";
                sucessMessage = "";
            }

        }

    }

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }

    public void delete(Picture picture) {
        int idx = picture.getUrl().lastIndexOf("/") + 1;
        String fileName = picture.getUrl().substring(idx);
        FtpUtil.delete("img/slinder", fileName);
        PictureModel.delete(picture.getIdPicture());
    }

}
