package com.jjmsoftsolutions.bean;

import com.jjmsoftsolutions.domain.Tag;
import com.jjmsoftsolutions.entity.Video;
import com.jjmsoftsolutions.entity.VideoComments;
import com.jjmsoftsolutions.model.VideoModel;
import com.jjmsoftsolutions.utils.JSFUtil;
import com.jjmsoftsolutions.utils.Session;
import com.jjmsoftsolutions.utils.StringUtils;
import com.jjmsoftsolutions.utils.Utility;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "VideoBean")
@ViewScoped
public class VideoBean implements Serializable {

    private Video video;
    private int totalVideos;
    private List<Video> music;
    private List<Video> popularSermonList;

    private VideoComments comment = new VideoComments();

    public Video getLastVideo() {
        setVideo(VideoModel.getLastVideo());
        return getVideo();
    }

    public void postComment() {
        comment.setCommentDate(new Date());
        comment.setEmail(Session.getUser());
        comment.setIdVideo(video);
        VideoModel.postComment(comment);
        Utility.redirect("Predicas?id=" + video.getIdVideo());
    }

    public void deletePostComment(int id) {
        VideoModel.delete(id);
        Utility.redirect("Predicas?id=" + video.getIdVideo());
    }

    public List<Video> getVideosIndex() {
        List<Video> list = VideoModel.getVideos("Predicas", 8);
        return list;
    }

    public List<Video> getPopularSermon() {
        popularSermonList = VideoModel.getPopularSermon("Predicas", 8);
        return popularSermonList;
    }

    public List<Video> getMusicIndex() {
        setMusic(VideoModel.getVideos("Musica", 9));
        return getMusic();
    }

    public Video getVideoById() {
        setVideo(VideoModel.getVideoById(StringUtils.getInt(JSFUtil.getParameterURL("id"))));
        return getVideo();
    }

    public List<Video> getAllVideosMusic() {
        List<Video> list = VideoModel.getAllVideosByAlbum("Musica");
        return list;
    }

    public List<Video> getAllVideosSermon() {
        String category = "Predicas";
        int end = StringUtils.getInt(JSFUtil.getParameterURL("end"));
        List<Video> list = VideoModel.getVideosByCategoryPagination(category, end);
        return list;
    }

    public List<Tag> getTotalButtons() {
        int pagination = StringUtils.getInt(JSFUtil.getParameterURL("end"));
        int totalRecord = VideoModel.getCountByCategory("Predicas");
        return Utility.getTag(3, totalRecord, pagination);
    }

    public List<Tag> getTotalCommentsVideos() {
        int totalRecord = VideoModel.getCountCommentsByBlog(JSFUtil.getParameterURL("id"));
        int pagination = StringUtils.getInt(JSFUtil.getParameterURL("sermon"));        
        return Utility.getTag(3, totalRecord, pagination);
    }

    public List<VideoComments> getCommentsByBlog() {
        String email = JSFUtil.getParameterURL("id");
        int totalRecords = StringUtils.getInt(JSFUtil.getParameterURL("sermon"));
        List<VideoComments> list = VideoModel.getCommentsByBlog(email, totalRecords);
        return list;
    }

    /**
     * @return the video
     */
    public Video getVideo() {
        return video;
    }

    /**
     * @param video the video to set
     */
    public void setVideo(Video video) {
        this.video = video;
    }

    /**
     * @return the music
     */
    public List<Video> getMusic() {
        return music;
    }

    /**
     * @param music the music to set
     */
    public void setMusic(List<Video> music) {
        this.music = music;
    }

    /**
     * @return the popularSermonList
     */
    public List<Video> getPopularSermonList() {
        return popularSermonList;
    }

    /**
     * @param popularSermonList the popularSermonList to set
     */
    public void setPopularSermonList(List<Video> popularSermonList) {
        this.popularSermonList = popularSermonList;
    }

    /**
     * @return the comment
     */
    public VideoComments getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(VideoComments comment) {
        this.comment = comment;
    }

}
