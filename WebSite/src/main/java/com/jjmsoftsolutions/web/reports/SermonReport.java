/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.web.reports;

import com.jjmsoftsolutions.domain.Email;
import com.jjmsoftsolutions.entity.Blog;
import com.jjmsoftsolutions.entity.Video;
import com.jjmsoftsolutions.model.BlogModel;
import com.jjmsoftsolutions.model.VideoModel;
import com.jjmsoftsolutions.utils.Constants;
import com.jjmsoftsolutions.utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Your Name <your.name at your.org>
 */
public class SermonReport {

    private static final String DOMAIN_NAME = "http://jjmsoftsolutions.com/Predicas?id=";
    
    public static void send(){
          String subject = "Boletín Prédicas";

        List<Blog> list = BlogModel.getAllBlogs();
        List<String> success = new ArrayList<String>();
        List<String> error = new ArrayList<String>();
        String message = getEmailSermon();
        for (Blog blog : list) {
            if (blog.getProperties() != null && blog.getProperties().getSendEmailSermon().equals(Constants.Y)) {
                boolean send = Email.send(blog.getEmail(), subject, message, "");
                if (send) {
                    success.add(blog.getEmail());
                    System.err.println("SUCCESS" + blog.getEmail());
                } else {
                    error.add(blog.getEmail());
                    System.err.println("ERROR" + blog.getEmail());
                }
            } else {
                System.err.println("DON'T SEND" + blog.getEmail());
            }
        }
    }

    public static String getEmailSermon() {
        StringBuilder email = new StringBuilder();
        email.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
        email.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
        email.append("   <head>\n");
        email.append("   <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n");
        email.append("   <title>Micine</title>\n");
        email.append("\n");
        email.append("   <style type=\"text/css\">\n");
        email.append("\n");
        email.append("   body{width:100%;margin:0px;padding:0px;background:#ffffff;text-align:center; line-height:0px; background:#25ad9f}\n");
        email.append("   html{width: 100%; }\n");
        email.append("   img {border:0px;text-decoration:none;display:block; outline:none;}\n");
        email.append("   a,a:hover{color:#ff6f6f;text-decoration:none;}.ReadMsgBody{width: 100%; background-color: #ffffff;}.ExternalClass{width: 100%; background-color: #ffffff;}\n");
        email.append("   table{ border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;  max-width:100%; }  \n");
        email.append("   .logo-space{padding:0px 33px 0px 0px;}\n");
        email.append("   .menu-space{padding:0px 33px 0px 40px;}\n");
        email.append("   .main-bg{ background:#ffffff;}\n");
        email.append("   .menu-text{ font-size:13px; font-weight:bold;}\n");
        email.append("			\n");
        email.append("			.see-btn{\n");
        email.append("				margin-top: 10px;\n");
        email.append("				font-size: 18px;\n");
        email.append("			}\n");
        email.append("			\n");
        email.append("			.btn{					\n");
        email.append("				background: #d2525a;\n");
        email.append("				color: #FFF;\n");
        email.append("				font-size: 16px; \n");
        email.append("				font-weight: bold; 		\n");
        email.append("				border: 1px solid #d9d9d9;\n");
        email.append("				border-radius: 3px;\n");
        email.append("				border-color: #d2525a;\n");
        email.append("				display: block;\n");
        email.append("   border: 1px solid #d9d9d9;\n");
        email.append("				width: 100px;\n");
        email.append("				height: 25px;\n");
        email.append("				padding: 3px 6px;	\n");
        email.append("			}\n");
        email.append("			\n");
        email.append("			\n");
        email.append("			\n");
        email.append("			.btn:hover{		\n");
        email.append("		\n");
        email.append("				color: #ffffff;\n");
        email.append("				background-color: #d2322d;\n");
        email.append("				border-color: #b92c28;\n");
        email.append("				\n");
        email.append("			\n");
        email.append("			}\n");
        email.append("\n");
        email.append("   @media only screen and (max-width:640px)\n");
        email.append("\n");
        email.append("   {\n");
        email.append("   .main {width:440px !important;}\n");
        email.append("   .two-column {width:440px !important;}\n");
        email.append("   .two-column-inner {width:420px !important; margin:0px auto;}\n");
        email.append("   .logo {width: 100%!important; text-align:center;}\n");
        email.append("   .menu {width: 100%!important; text-align:center;}\n");
        email.append("   .full {width: 100%!important;}\n");
        email.append("\n");
        email.append("\n");
        email.append("   }\n");
        email.append("\n");
        email.append("   @media only screen and (max-width:479px)\n");
        email.append("   {\n");
        email.append("   .main {width:280px !important;}\n");
        email.append("   .two-column {width:280px !important;}\n");
        email.append("   .two-column-inner {width:260px !important; margin:0px auto;}\n");
        email.append("   .logo {width: 100%!important; text-align:center;}\n");
        email.append("   .menu {width: 100%!important; text-align:center;}\n");
        email.append("   .logo-space,.menu-space{padding:40px 0px 34px 0px;}\n");
        email.append("   .full {width: 100%!important;}\n");
        email.append("   .menu-text{ font-size:11px; font-weight:normal;}\n");
        email.append("\n");
        email.append("\n");
        email.append("   }\n");
        email.append("\n");
        email.append("\n");
        email.append("\n");
        email.append("\n");
        email.append("   }\n");
        email.append("\n");
        email.append("   </style>\n");
        email.append("   </head>\n");
        email.append("\n");
        email.append("   <body>\n");
        email.append("\n");

        List<Video> videos = VideoModel.getVideoByDateCategory(DateUtils.getBeginOfCurrentMonth(), DateUtils.getEndOfCurrentMonth(), "Predicas");

        int size = 0;
        if (videos != null) {
            size = videos.size();
        }

        email.append("<center>\n");
        email.append("   <table style=\"background: #FFFFFF;\">\n");
        email.append("   <tr>\n");
        email.append("   <td>\n");
        email.append("   <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"padding:0px 0px 20px 0px;\"><table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"main\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"left\" valign=\"top\" style=\"padding-bottom:34px;\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/top-border.png\" width=\"650\" height=\"5\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" /></td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"left\" valign=\"top\">\n");
        email.append("\n");
        email.append("   <!--View Browser part Start-->\n");
        email.append("   <table border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" class=\"two-column\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\"><table width=\"270\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td width=\"105\" align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("<tr>\n");
        email.append("<td width=\"8%\" align=\"left\" valign=\"top\">\n");
        email.append("																				<img src=\"http://162.253.126.108/~ucbdpdbn/public/img/logo/+Q.png\"  width=\"25\" height=\"25\" alt=\"\" />\n");
        email.append("																			</td>\n");
        email.append("																			\n");
        email.append("																			 <td width=\"92%\" align=\"left\" valign=\"middle\" style=\"font:Bold 11px Arial, Helvetica, sans-serif; color:#949191;padding-left:6px;\"><a href=\"#\" style=\" color:#d2525a;\">\n");
        email.append("																			 Boletín +Q Joven (Prédicas)</a>\n");
        email.append("																			 </td>\n");
        email.append("\n");
        email.append("</tr>\n");
        email.append("</table></td>\n");
        email.append("  \n");
        email.append("   </tr>\n");
        email.append("   </table></td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"padding-top:4px;\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/space.png\" width=\"4\" height=\"4\" alt=\"\" /></td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   <!--View Browser part End-->\n");
        email.append("\n");
        email.append("   <table border=\"0\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\" class=\"two-column\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\">\n");
        email.append("\n");
        email.append("   <!--Date Start-->\n");
        email.append("   <table  border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" style=\"font:Bold 18px Arial, Helvetica, sans-serif; color:#949191; padding-left:6px;\">\n");
        email.append("																<span style=\"font-size:12px; color:#d2525a;\">\n");
        email.append("Boletín del Mes de \n");
        email.append("</span> \n");
        email.append(DateUtils.getMonthName(new Date())).append(" del ").append(DateUtils.getYear(new Date())).append("\n");
        email.append("</td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   <!--Date End-->\n");
        email.append("\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("\n");
        email.append("   </table></td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\">\n");
        email.append("\n");
        email.append(" \n");
        email.append("\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"padding-bottom:45px;\">\n");
        email.append("\n");
        email.append("   <!--Banner Start-->\n");
        email.append("   <table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"main\">\n");

        if (size >= 1) {
//- BEGIN IMAGE-//
            email.append("<tr>\n");
            Video video1 = videos.get(0);
            email.append("<td align=\"center\" valign=\"top\">");
            email.append("<img src=\"").append(video1.getImage()).append("\" width=\"650\" height=\"433\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" />");
            email.append("</td>\n");
            email.append("</tr>\n");
//- END IMAGE-//

            email.append("<tr>\n");
            email.append("<td align=\"center\" valign=\"top\" style=\"padding:35px 0px 25px 0px;\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");

//- BEGIN TITLE-//
            email.append("<tr>\n");
            email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 26px Arial, Helvetica, sans-serif; color:#606060; line-height:32px;\">").append(video1.getTitle()).append("</td>\n");
            email.append("</tr>\n");
//- END TITLE-//

//- BEGIN DESCRIPTION-//
            email.append("<tr>\n");
            email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 12px Arial, Helvetica, sans-serif; color:#858485; line-height:20px; padding-top:12px;\">").append(video1.getDescription()).append("</td>\n");
            email.append("</tr>\n");
//- END DESCRIPTION-//

            email.append("</table></td>\n");
            email.append("</tr>\n");

//- BEGIN BUTTON -//
            email.append("<tr>\n");
            email.append("<td align=\"center\" valign=\"top\">\n");
            email.append("<a href=\"").append(DOMAIN_NAME.concat("" + video1.getIdVideo())).append("\" class=\"btn\">\n");
            email.append("<div class=\"see-btn\">\n");
            email.append("Ver\n");
            email.append("</div>\n");
            email.append("\n");
            email.append("\n");
            email.append("</a>\n");
            email.append("</td>\n");
            email.append("</tr>\n");
//- END BUTTON -//

            email.append("</table>\n");
            email.append("<!--Banner End-->\n");

            email.append("\n");
            email.append("</td>\n");
            email.append("</tr>\n");
            email.append("</table>\n");
            email.append("<!--Header End-->   \n");
            email.append("\n");
            email.append("\n");
        }

        if (size >= 2) {
            email.append("<!--3-column-part Start-->\n");
            email.append("   <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"center\" valign=\"top\"><table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"main\">\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"left\" valign=\"top\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/border.png\" width=\"650\" height=\"39\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" /></td>\n");
            email.append("   </tr>\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"center\" valign=\"top\">\n");
            email.append("\n");

//- BEGIN SECOND TITLE TO DISPLAY THE OTHERS VIDEOS --//
            email.append("   <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"center\" valign=\"top\" style=\"font:Normal 26px Arial, Helvetica, sans-serif; color:#d2525a; line-height:32px; text-transform:uppercase;\">Otras Prédicas</td>\n");
            email.append("   </tr>\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"center\" valign=\"top\" style=\"font:Normal 12px Arial, Helvetica, sans-serif; color:#858485; line-height:20px; padding-top:4px;\">Observa más de nuestras prédicas</td>\n");
            email.append("   </tr>\n");
            email.append("   </table>\n");
            email.append("\n");
//- END SECOND TITLE TO DISPLAY THE OTHERS VIDEOS --//

            email.append("   </td>\n");
            email.append("   </tr>\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"left\" valign=\"top\" style=\"padding:50px 0px 60px 0px;\"><table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"main\">\n");
            email.append("   <tr>\n");
            email.append("   <td align=\"center\" valign=\"top\">\n");
            email.append("\n");

            email.append("<table width=\"432\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" class=\"two-column\">\n");
            email.append("<tr>\n");
            email.append("<td align=\"left\" valign=\"top\">\n");
            email.append("\n");

            if (size >= 2) {
// BEGIN VIDEO 2 //

                email.append("<!--3-column-1 Start-->\n");
                email.append("<table width=\"213\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" class=\"two-column\">\n");

                Video video2 = videos.get(1);
// BEGIN DISPLAY IMAGE
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">");
                email.append("<img src=\"").append(video2.getImage()).append("\" width=\"213\" height=\"149\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" />");
                email.append("</td>\n");
                email.append("</tr>\n");
// END DISPLAY IMAGE

                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"padding:14px 0px 08px 0px;\"><table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");

// BEGIN TITLE
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 18px Arial, Helvetica, sans-serif; color:#353333; line-height:32px;\">").append(video2.getTitle()).append("</td>\n");
                email.append("</tr>\n");
// END TITLE

//BEGIN DESCRIPTION
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 12px Arial, Helvetica, sans-serif; color:#858485; line-height:22px; padding:4px 2px 0px 2px;\">").append(video2.getDescription()).append("</td>\n");
                email.append("</tr>\n");
//END DESCRIPTION

                email.append("</table></td>\n");

                email.append("</tr>\n");
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">\n");

                email.append("<a href=\"").append(DOMAIN_NAME.concat("" + video2.getIdVideo())).append("\" class=\"btn\">\n");
                email.append("<div class=\"see-btn\">\n");
                email.append("Ver\n");
                email.append("</div>\n");
                email.append("</a>\n");
                email.append("</td>\n");
                email.append("</tr>\n");
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">&nbsp;</td>\n");
                email.append("</tr>\n");
                email.append("</table>\n");
                email.append("<!--3-column-1 End-->\n");
                // END VIDEO 2 //

            }

            if (size >= 3) {

                email.append("<!--3-column-2 Start-->\n");
                email.append("<table width=\"213\" border=\"0\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\" class=\"two-column\">\n");

                Video video3 = videos.get(2);
                //BEGIN IMAGE
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">\n");
                email.append("<img src=\"").append(video3.getImage()).append("\" width=\"213\" height=\"149\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" />\n");
                email.append("</td>\n");
                email.append("</tr>\n");
                //END IMAGE

                //BEGIN TITLE
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"padding:14px 0px 08px 0px;\"><table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 18px Arial, Helvetica, sans-serif; color:#353333; line-height:32px;\">").append(video3.getTitle()).append("</td>\n");
                email.append("</tr>\n");
                //END TITLE

                //BEGIN DESCRIPTION
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 12px Arial, Helvetica, sans-serif; color:#858485; line-height:22px; padding:4px 2px 0px 2px;\">").append(video3.getDescription()).append("</td>\n");
                email.append("</tr>\n");
                email.append("</table></td>\n");
                email.append("</tr>\n");
                //END DESCRIPTION

                //BEGIN BUTTON
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\"><a href=\"#\">\n");
                email.append("<a href=\"").append(DOMAIN_NAME.concat("" + video3.getIdVideo())).append("\" class=\"btn\">\n");
                email.append("<div class=\"see-btn\">\n");
                email.append("Ver\n");
                email.append("</div>\n");
                email.append("</a>\n");
                email.append("</td>\n");
                email.append("</tr>\n");
                //END BUTTON

                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">&nbsp;</td>\n");
                email.append("</tr>\n");
                email.append("</table>\n");
                email.append("<!--3-column-2 End-->\n");
                email.append("\n");
                email.append("</td>\n");
                email.append("</tr>\n");
                email.append("</table>\n");
                email.append("\n");
                email.append("\n");
                email.append("\n");
                email.append("\n");

            }

            if (size >= 4) {
                Video video4 = videos.get(3);
                email.append("<!--3-column-3 Start-->\n");
                email.append("<table width=\"213\" border=\"0\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\" class=\"two-column\">\n");

                //BEGIN IMAGE
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">");
                email.append("<img src=\"").append(video4.getImage()).append("\" width=\"213\" height=\"149\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" />");
                email.append("</td>\n");
                email.append("</tr>\n");
                //END IMAGE

                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"padding:14px 0px 08px 0px;\"><table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");

                //BEGIN TITLE
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 18px Arial, Helvetica, sans-serif; color:#353333; line-height:32px;\">").append(video4.getTitle()).append("</td>\n");
                email.append("</tr>\n");
                //END TITLE

                //BEGIN DESCRIPTION
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\" style=\"font:Normal 12px Arial, Helvetica, sans-serif; color:#858485; line-height:22px; padding:4px 2px 0px 2px;\">").append(video4.getDescription()).append("</td>\n");
                email.append("</tr>\n");
                //END DESCRIPTION

                email.append("</table></td>\n");
                email.append("   </tr>\n");
                email.append("   <tr>\n");
                email.append("   <td align=\"center\" valign=\"top\">\n");

                email.append("<a href=\"").append(DOMAIN_NAME.concat("" + video4.getIdVideo())).append("\" class=\"btn\">\n");
                email.append("<div class=\"see-btn\">\n");
                email.append("Ver\n");
                email.append("</div>\n");
                email.append("</a>\n");
                email.append("</td>\n");
                email.append("</tr>\n");
                email.append("<tr>\n");
                email.append("<td align=\"center\" valign=\"top\">&nbsp;</td>\n");
                email.append("</tr>\n");
                email.append("</table>\n");
                email.append("<!--3-column-3 End-->\n");
                email.append("\n");
                email.append("   </td>\n");
                email.append("   </tr>\n");
                email.append("   </table></td>\n");
                email.append("   </tr>\n");
                email.append("   </table></td>\n");
                email.append("   </tr>\n");
                email.append("   </table>\n");
                email.append("   <!--3-column-part End-->\n");
            }

        }

        email.append("\n");
        email.append("   \n");
        email.append("\n");
        email.append("   \n");
        email.append("\n");
        email.append("\n");
        email.append("\n");
        email.append("   <!--Footer Start-->\n");
        email.append("\n");
        email.append("   <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\">\n");
        email.append("\n");
        email.append("   <table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"main\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"left\" valign=\"middle\" style=\"border-top:#efeded solid 1px; padding:23px 0px 35px 0px;\">\n");
        email.append("\n");
        email.append("   <!--copy-right part Start-->\n");
        email.append("   <table width=\"90%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"font:Normal 12px Arial, Helvetica, sans-serif; color:#858485; line-height:22px; padding-top:4px;\">\n");
        email.append("													Usted ha recibido este correo electrónico por que se ha afiliado para poder ver las últimas prédicas<br />\n");
        email.append("													Si desea no verlas favor cancele la suscripción.\n");
        email.append("												\n");
        email.append("													</td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\"><table  border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"padding-top:6px;\">\n");
        email.append("																	<img src=\"http://freetemplates.bz/design/micine/preview/red/images/space.png\" width=\"4\" height=\"4\" alt=\"\" />\n");
        email.append("																</td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"font:Bold 14px Arial, Helvetica, sans-serif; color:#d2525a;\">\n");
        email.append("																Copyright &copy; 2014 +Q Joven.com\n");
        email.append("																</td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"padding-top:6px;\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/space.png\" width=\"4\" height=\"4\" alt=\"\" /></td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"center\" valign=\"top\" style=\"font:Bold 14px Arial, Helvetica, sans-serif; color:#d2525a; line-height:32px;\">\n");
        email.append("\n");
        email.append("   <!--social media part Start-->\n");
        email.append("   <table width=\"170\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"left\" valign=\"top\"><a href=\"#\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/facebook.png\" width=\"32\" height=\"32\" alt=\"\" /></a></td>\n");
        email.append("   <td align=\"left\" valign=\"top\"><a href=\"#\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/twitter.png\" width=\"32\" height=\"32\" alt=\"\" /></a></td>\n");
        email.append("   <td align=\"left\" valign=\"top\"><a href=\"#\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/google+.png\" width=\"32\" height=\"32\" alt=\"\" /></a></td>\n");
        email.append("   <td align=\"left\" valign=\"top\"><a href=\"#\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/youtube.png\" width=\"32\" height=\"32\" alt=\"\" /></a></td>\n");
        email.append("   <td align=\"left\" valign=\"top\"><a href=\"#\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/dribbble.png\" width=\"32\" height=\"32\" alt=\"\" /></a></td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   <!--social media part End-->\n");
        email.append("\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   <!--copy-right part End-->\n");
        email.append("\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   <tr>\n");
        email.append("   <td align=\"left\" valign=\"top\"><img src=\"http://freetemplates.bz/design/micine/preview/red/images/top-border.png\" width=\"650\" height=\"5\" alt=\"\" style=\"display:block;width:100% !important; height:auto !important; \" /></td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   </table>\n");
        email.append("   <!--Footer End-->\n");
        email.append("   </td>\n");
        email.append("   </tr>\n");
        email.append("   <!--Main Table End-->\n");
        email.append("   </table>\n");
        email.append("		 </center>\n");
        email.append("   </body>\n");
        email.append("</html>");

        Email.send("jonathanjm100@hotmail.com", "subject", email.toString(), "");
        Email.send("jonathanjm100@gmail.com", "subject", email.toString(), "");
        Email.send("jonathanjm101@yahoo.es", "subject", email.toString(), "");

        return email.toString();

    }

}
