/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.domain.Verse;
import com.jjmsoftsolutions.utils.XMLUtils;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "verseController")
public class VerseController implements Serializable {

    private final String URL = "http://www.biblegateway.com/usage/votd/rss/votd.rdf?$version_id?42";

    public Verse getVerse() {

        Verse verse = new Verse();

        try {
            URL url = new URL(URL);
            URLConnection connection = url.openConnection();
            Document doc = XMLUtils.parseXML(connection.getInputStream());
            NodeList descNodes = doc.getElementsByTagName("content:encoded");
            NodeList descNodeTitle = doc.getElementsByTagName("title");
            verse.setVerse(getVerse(descNodes));
            verse.setBook(descNodeTitle.item(1).getTextContent());
        } catch (MalformedURLException ex) {
            Logger.getLogger(VerseController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VerseController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(VerseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return verse;
    }

    private String getVerse(NodeList descNodes) {
        String verse = descNodes.item(0).getTextContent()
                .replaceAll("&ldquo;", "")
                .replaceAll("&rdquo;", "")
                .replaceAll("Brought to you", "")
                .replaceAll("by <a href=\"http://www.biblegateway.com\">", "")
                .replaceAll("</a>.", "")
                .replaceAll("NVI.", "")
                .replaceAll("BibleGateway.com", "")
                .replaceAll("Copyright", "")
                .replaceAll("\\([^\\(]*\\)", "")
                .replaceAll("All Rights Reserved.", "")
                .replaceAll("\\[.+\\]", "");
        return verse;
    }

}
