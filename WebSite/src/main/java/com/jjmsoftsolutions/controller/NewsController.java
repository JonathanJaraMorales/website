/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.controller;

import com.jjmsoftsolutions.domain.News;
import com.jjmsoftsolutions.model.NewsModel;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author jonathan
 */
@ManagedBean(name = "newsController")
public class NewsController {

    public List<News> getNewsList() {
        List<News> list = new NewsModel().getNewList();
        System.err.println("LIST" + list.size());
        return list;

    }
}
