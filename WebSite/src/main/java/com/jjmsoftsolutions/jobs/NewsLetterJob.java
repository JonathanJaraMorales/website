package com.jjmsoftsolutions.jobs;

import com.jjmsoftsolutions.web.reports.SermonReport;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class NewsLetterJob implements Job {

    @Override
    public void execute(final JobExecutionContext ctx) throws JobExecutionException {        
        System.out.println("Executing Sermon Job");
        SermonReport.send();
    }

}
