package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import static com.jjmsoftsolutions.dataacces.DataAccess.findAll;
import com.jjmsoftsolutions.entity.Picture;
import com.jjmsoftsolutions.entity.PictureAlbum;
import java.util.HashMap;
import java.util.List;

public class PictureAlbumModel {

    public static PictureAlbum getAlbum(String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM PictureAlbum U ");
        sql.append("WHERE U.category = :category ");
        HashMap parameters = new HashMap();
        parameters.put("category", category);
        return (PictureAlbum) DataAccess.find(PictureAlbum.class, sql.toString(), parameters);
    }

    public static List<PictureAlbum> getAllAlbums() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From PictureAlbum V ");
        sql.append("WHERE V.category <> :category ");
        sql.append("AND V.category <> :category2 ");
        HashMap parameters = new HashMap();
        parameters.put("category", "Slinder");
        parameters.put("category2", "min-coreografia");
        sql.append("ORDER BY V.idPictureAlbum DESC ");
        List<PictureAlbum> list = (List) DataAccess.findAll(PictureAlbum.class, sql.toString(), parameters);
        return list;
    }

    public static List<PictureAlbum> getRecentAlbums(int max) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From PictureAlbum V ");
        sql.append("WHERE V.category <> :category ");
        sql.append("AND V.category <> :category2 ");
        HashMap parameters = new HashMap();
        parameters.put("category", "Slinder");
        parameters.put("category2", "min-coreografia");
        sql.append("ORDER BY V.idPictureAlbum DESC ");
        List<PictureAlbum> list = (List) DataAccess.findAll(PictureAlbum.class, sql.toString(), parameters, max);
        return list;
    }

    public static PictureAlbum getAlbumById(int id) {
        return (PictureAlbum) DataAccess.find(PictureAlbum.class, id);
    }

    

}
