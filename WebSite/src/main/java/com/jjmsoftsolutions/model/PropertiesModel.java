package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Properties;

public class PropertiesModel {

    public static int getSlinderPagination() {
        return ((Properties)DataAccess.find(Properties.class, 1)).getSlinderPagination();
    }
    
    public static Properties getProperties(){
         return ((Properties)DataAccess.find(Properties.class, 1));
    }

    public static void update(Properties properties) {
        DataAccess.update(properties);
    }
    
}
