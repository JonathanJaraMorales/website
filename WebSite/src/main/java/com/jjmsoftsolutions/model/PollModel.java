package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.domain.Statistic;
import com.jjmsoftsolutions.entity.Poll;
import com.jjmsoftsolutions.entity.PollStats;
import com.jjmsoftsolutions.utils.Session;
import com.jjmsoftsolutions.utils.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PollModel {

    public static Poll getLastPoll() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Poll V ");
        sql.append("ORDER BY V.idPoll DESC ");
        return (Poll) DataAccess.findTop(Poll.class, sql.toString());
    }

    public static void setVote(PollStats stats) {
        DataAccess.insert(stats);
    }

    public static List<Statistic> getStatisTic(Integer idPoll) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT TRUNCATE((COUNT(ID_POLL_OPTION)/(SELECT COUNT(ID_POLL_OPTION) ");
        sql.append("FROM POLL_STATS A ");
        sql.append("JOIN POLL_OPTIONS B ON A.ID_POLL_OPTION = B.ID_POLL_OPTIONS ");
        sql.append("WHERE B.ID_POLL = ").append(idPoll).append(") * 100 ),2) AS PERCENTAGE, DESCRIPTION ");
        sql.append("FROM POLL_STATS A ");
        sql.append("JOIN POLL_OPTIONS B ON A.ID_POLL_OPTION = B.ID_POLL_OPTIONS ");
        sql.append("WHERE B.ID_POLL = ").append(idPoll).append(" ");
        sql.append("GROUP BY DESCRIPTION ");
        sql.append("ORDER BY PERCENTAGE DESC ");
        List<Object[]> resultList = DataAccess.getNativeQuery(sql.toString());
        List<Statistic> list = new ArrayList<Statistic>();
        for (Object[] obj : resultList) {
            Statistic stat = new Statistic();
            stat.setDescription(StringUtils.getString(obj[1]));
            stat.setPercentage(StringUtils.getDouble(obj[0]));
            list.add(stat);
        }
        return list;
    }

    public static PollStats userVote(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From PollStats V ");
        sql.append("WHERE V.email.email = :email ");
        sql.append("AND V.idPoll.idPoll = :id ");     
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("email", Session.getUser().getEmail());
        map.put("id", id);        
        List<PollStats> list = (List) DataAccess.findAll(PollStats.class, sql.toString(), map);
        return list.size() > 0 ? list.get(0) : null;
    }

    public static void delete(Integer id) {
        DataAccess.delete(PollStats.class, id);
    }
}
