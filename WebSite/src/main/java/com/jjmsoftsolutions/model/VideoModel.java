package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Video;
import com.jjmsoftsolutions.entity.VideoAlbum;
import com.jjmsoftsolutions.entity.VideoComments;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class VideoModel {

    public static Video getLastVideo() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Video V ");
        sql.append("ORDER BY V.idVideo DESC ");
        return (Video) DataAccess.findTop(Video.class, sql.toString());
    }

    public static Video getVideoById(int id) {
        return (Video) DataAccess.find(Video.class, id);
    }

    public static List<Video> getAllVideosByAlbum(String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT A From VideoAlbum A ");
        sql.append("WHERE A.category = :category ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("category", category);
        VideoAlbum album = (VideoAlbum) DataAccess.find(VideoAlbum.class, sql.toString(), map);
        return album != null ? album.getVideoList() : null;
    }

    public static List<Video> getVideos(String category, int totalRows) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT A From Video A ");
        sql.append("WHERE A.idVideoAlbum.category = :category ");
        sql.append("ORDER BY A.idVideo DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("category", category);
        List<Video> videos = (List) DataAccess.findAll(Video.class, sql.toString(), map, totalRows);
        return videos;
    }

    public static List<Video> getPopularSermon(String category, int totalRows) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT A From Video A ");
        sql.append("WHERE A.idVideoAlbum.category = :category ");
        sql.append("ORDER BY A.idVideo DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("category", category);
        List<Video> videos = (List) DataAccess.findAll(Video.class, sql.toString(), map, totalRows);
        return videos;
    }

    public static void postComment(VideoComments comment) {
        DataAccess.insert(comment);
    }

    public static void delete(int id) {
        DataAccess.delete(VideoComments.class, id);
    }

    public static List<Video> getVideosByCategoryPagination(String category, int totalRows) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT A From Video A ");
        sql.append("WHERE A.idVideoAlbum.category = :category ");
        sql.append("ORDER BY A.idVideo DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("category", category);
        List<Video> videos = (List) DataAccess.findAllPagination(Video.class, sql.toString(), map, totalRows - 3, totalRows);
        return videos;
    }

    public static int getCountByCategory(String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(u) FROM Video u ");
        sql.append("WHERE u.idVideoAlbum.category = :category ");
        sql.append("ORDER BY u.idVideo DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("category", category);
        return DataAccess.count(sql.toString(), map);
    }

    public static int getCountCommentsByBlog(String email) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(u) FROM VideoComments u ");
        sql.append("WHERE u.email.email = :email ");
        sql.append("ORDER BY u.idVideoComment DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", email);
        return DataAccess.count(sql.toString(), map);

    }

    public static List<VideoComments> getCommentsByBlog(String email, int totalRecords) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT A From VideoComments A ");
        sql.append("WHERE A.email.email = :email ");
        sql.append("ORDER BY A.idVideoComment DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", email);
        List<VideoComments> videos = (List) DataAccess.findAllPagination(VideoComments.class, sql.toString(), map, totalRecords - 3, 3);
        return videos;
    }

    public static List<Video> getVideoByDateCategory(Date start, Date end, String category) {

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT A From Video A ");
        sql.append("WHERE A.idVideoAlbum.category = :category ");
        sql.append("AND A.videoDate BETWEEN :toDate AND :fromDate ");
        sql.append("ORDER BY A.videoDate DESC ");
        
        HashMap map = new HashMap();
        map.put("category", category);
        map.put("toDate", start);
        map.put("fromDate", end);

        List<Video> videos = (List) DataAccess.findAll(Video.class, sql.toString(), map);

        return videos;

    }

}
