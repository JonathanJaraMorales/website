package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Post;
import com.jjmsoftsolutions.entity.PostComment;
import java.util.HashMap;
import java.util.List;

public class PostModel {

    public static Post getLastPost(String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Post V ");
        sql.append("WHERE V.idPostCategory.name = :name ");
        sql.append("ORDER BY V.idPost DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", category);
        map.put("name1", "Eventos");
        map.put("name2", "Curiosidades");
        return (Post) DataAccess.findTop(Post.class, sql.toString(), map);
    }

    public static List<Post> getPopularPost(int totalRows) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Post V ");
        sql.append("WHERE V.idPostCategory.name <> :name1 ");
        sql.append("AND V.idPostCategory.name <> :name2 ");
        sql.append("ORDER BY V.idPost DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name1", "Eventos");
        map.put("name2", "Curiosidades");
        List<Post> list = (List) DataAccess.findAll(Post.class, sql.toString(), map, totalRows);
        return list;
    }

    public static List<Post> getAllPostByCategory(String category, int totalRecords) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Post V ");
        sql.append("WHERE V.idPostCategory.name = :name1 ");
        sql.append("ORDER BY V.idPost DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name1", category);
        List<Post> list = (List) DataAccess.findAll(Post.class, sql.toString(), map, totalRecords);
        return list;
    }

    public static List<Post> getPostByCategory(String category, int totalRows) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Post V ");
        sql.append("WHERE V.idPostCategory.name = :name ");
        sql.append("ORDER BY V.idPost DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", category);
        List<Post> list = (List) DataAccess.findAll(Post.class, sql.toString(), map, totalRows);
        return list;
    }

    public static Post getPostById(int id) {
        return (Post) DataAccess.find(Post.class, id);
    }

    public static void postComment(PostComment comment) {
        DataAccess.insert(comment);
    }

    public static void delete(int id) {
        DataAccess.delete(PostComment.class, id);
    }

    public static int getCountByCategory(String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(u) FROM Post u ");
        sql.append("WHERE u.idPostCategory.name = :category ");
        sql.append("ORDER BY u.idPost DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("category", category);
        return DataAccess.count(sql.toString(), map);
    }

    public static Post getPostByIdCategory(int id, String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From Post V ");
        sql.append("WHERE V.idPostCategory.name = :name ");
        sql.append("AND V.idPost = :id ");
        sql.append("ORDER BY V.idPost DESC ");
        HashMap map = new HashMap();
        map.put("name", category);
        map.put("id", id);
        return (Post) DataAccess.find(Post.class, sql.toString(), map);
    }

    public static List<PostComment> getAllPostByCommentsByBlog(String email, int records) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT V From PostComment V ");
        sql.append("WHERE V.email.email = :email ");
        sql.append("ORDER BY V.idPostComment DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", email);
        List<PostComment> list = (List) DataAccess.findAllPagination(PostComment.class, sql.toString(), map, records - 3, records);
        return list;
    }

     public static int getCountCommentsByBlog(String email) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(u) FROM PostComment u ");
        sql.append("WHERE u.email.email = :email ");
        sql.append("ORDER BY u.idPostComment DESC ");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("email", email);
        return DataAccess.count(sql.toString(), map);

    }
    

}
