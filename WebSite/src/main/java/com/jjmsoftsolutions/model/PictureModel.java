package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Picture;
import com.jjmsoftsolutions.entity.PictureAlbum;
import java.util.HashMap;
import java.util.List;

public class PictureModel {

    public static void insert(Picture picture) {
        DataAccess.insert(picture);
    }

    public static void delete(int id) {
        DataAccess.delete(Picture.class, id);
    }

    public static List<Picture> getPicturesByAlbum(String category, int pagination) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Picture U ");
        sql.append("WHERE U.idPictureAlbum.category = :category ");
        HashMap parameters = new HashMap();
        parameters.put("category", category);
        List<Picture> list = (List) DataAccess.findAll(Picture.class, sql.toString(), parameters, pagination);
        return list;
    }

    public static List<Picture> getPicturesByAlbum(String category) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Picture U ");
        sql.append("WHERE U.idPictureAlbum.category = :category ");
        HashMap parameters = new HashMap();
        parameters.put("category", category);
        List<Picture> list = (List) DataAccess.findAll(Picture.class, sql.toString(), parameters);
        return list;
    }

}
