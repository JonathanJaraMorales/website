/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.domain.News;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class NewsModel {

    public List<News> getNewList() {
        List<News> list = (List) DataAccess.findAll(News.class);
        System.err.println(list);
        return list;

    }

}
