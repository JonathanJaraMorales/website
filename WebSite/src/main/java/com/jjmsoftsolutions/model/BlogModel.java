package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import com.jjmsoftsolutions.entity.Blog;
import com.jjmsoftsolutions.entity.BlogSecurity;
import com.jjmsoftsolutions.entity.BlogSession;
import java.util.HashMap;
import java.util.List;

public class BlogModel {

    public static void insert(Blog blog) {

        Character option = '3';
        BlogSecurity security = new BlogSecurity();
        security.setShowEmail(option);
        security.setShowPhone(option);
        security.setShowActivity(option);
        security.setShowProfile(option);
        security.setShowLastConnection(option);
        security.setShowFirstName(option);
        security.setShowName(option);
        security.setShowLastName(option);
        security.setShowGender(option);
        security.setShowBirthDate(option);
        security.setShowStatusConecction(option);
        security.setShowSecondLastName(option);
        security.setEmail(blog);

        DataAccess.insert(blog);
        DataAccess.insert(security);
    }

    public static void insertSession(BlogSession blogSession) {
        DataAccess.insert(blogSession);
    }

    public static List<Blog> getBlogByPassword(String password) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Blog U ");
        sql.append("WHERE U.password = :password ");
        sql.append("AND U.activate = :activate ");
        HashMap parameters = new HashMap();
        parameters.put("password", password);
        parameters.put("activate", 'F');
        List<Blog> user = (List) DataAccess.findAll(Blog.class, sql.toString(), parameters);
        return user;
    }

    public static void update(Blog blog) {
        DataAccess.update(blog);
    }

    public static Blog getBlogByEmail(String email) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Blog U ");
        sql.append("WHERE U.email = :email ");
        HashMap parameters = new HashMap();
        parameters.put("email", email);
        return (Blog) DataAccess.find(Blog.class, sql.toString(), parameters);
    }

    public static boolean delete(Blog blog) {
        StringBuilder sql = new StringBuilder();

        sql.append("DELETE FROM BLOG_SESSION WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM VIDEO_COMMENTS WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM PICTURE_COMMENTS WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM POLL_STATS WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM POST WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM POST_COMMENT WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM BLOG_SECURITY WHERE EMAIL='").append(blog.getEmail()).append("' ");
        DataAccess.deleteNative(sql.toString());

        sql = new StringBuilder();
        sql.append("DELETE FROM BLOG WHERE EMAIL='").append(blog.getEmail()).append("' ");
        boolean result = DataAccess.deleteNative(sql.toString());

        return result;

    }

    public static void updateSecurity(BlogSecurity security) {
        DataAccess.update(security);

    }

    public static void updatePassword(Blog blog) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE Blog b SET b.picture = :picture ");
        sql.append("WHERE b.email = :email ");

        HashMap parameters = new HashMap();
        parameters.put("picture", blog.getPicture());
        parameters.put("email", blog.getEmail());

        DataAccess.update(sql.toString(), parameters);

    }

    public static void updatePicture(Blog blog) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE Blog b SET b.password = :password ");
        sql.append("WHERE b.email = :email ");

        HashMap parameters = new HashMap();
        parameters.put("password", blog.getPassword());
        parameters.put("email", blog.getEmail());

        DataAccess.update(sql.toString(), parameters);

    }

    public static List<Blog> getAllBlogs() {
        List<Blog> user = (List) DataAccess.findAll(Blog.class);
        return user;
    }

}
