package com.jjmsoftsolutions.model;

import com.jjmsoftsolutions.dataacces.DataAccess;
import static com.jjmsoftsolutions.dataacces.DataAccess.find;
import com.jjmsoftsolutions.entity.Blog;
import java.util.HashMap;

 /*
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 
 * Copyright 2014 JJMSoftSolutions
 */
public class LoginModel {

    /**
     * Validate if the user exist in the database
     * @param userName
     * @param password
     * @return 
     */
    public static Blog login(String userName, String password) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Blog U ");
        sql.append("WHERE U.username = :username ");
        sql.append("AND U.password = :password ");
        sql.append("AND U.activate = :activate ");
        
        HashMap parameters = new HashMap();
        parameters.put("username", userName);
        parameters.put("password", password);  
        parameters.put("activate", 'Y');  

        Blog user = (Blog) find(Blog.class, sql.toString(), parameters);

        return user;
    }
    
    public static Blog isDisable(String userName, String password) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT U FROM Blog U ");
        sql.append("WHERE U.username = :username ");
        sql.append("AND U.password = :password ");
        sql.append("AND U.activate = :activate ");
        
        HashMap parameters = new HashMap();
        parameters.put("username", userName);
        parameters.put("password", password);  
        parameters.put("activate", 'F');  

        Blog user = (Blog) find(Blog.class, sql.toString(), parameters);

        return user;
    }

    public static void update(Blog user) {
        DataAccess.update(user);
    }

}
