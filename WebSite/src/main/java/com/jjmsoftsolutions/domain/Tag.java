package com.jjmsoftsolutions.domain;


public class Tag {
    
    private int number;
    private boolean selected;
    
    public Tag(int number, boolean selected){
        setNumber(number);
        setSelected(selected);
    }

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * @return the selected
     */
    public boolean getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
}
