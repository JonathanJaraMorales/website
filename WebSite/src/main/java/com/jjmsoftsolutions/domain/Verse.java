/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.domain;

/**
 *
 * @author jonathan
 */
public class Verse {

    private String verse;
    private String book;
    private String version;

    /**
     * @return the verse
     */
    public String getVerse() {
        return verse;
    }

    /**
     * @param verse the verse to set
     */
    public void setVerse(String verse) {
        this.verse = verse;
    }

    /**
     * @return the book
     */
    public String getBook() {
        return book;
    }

    /**
     * @param book the book to set
     */
    public void setBook(String book) {
        this.book = book;
    }

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

}
