package com.jjmsoftsolutions.domain;

import com.jjmsoftsolutions.utils.Utility;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import static javax.mail.Session.getDefaultInstance;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {

    private static final Properties properties = new Properties();
    private static Session session;

    private static void init() {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.mail.sender", "masqjoven@gmail.com");
        properties.put("mail.smtp.password", "ccividaabundante");
        properties.put("mail.smtp.user", "masqjoven@gmail.com");
        properties.put("mail.smtp.auth", "true");
        session = getDefaultInstance(properties);
    }

    public static boolean send(String emailTo, String subject, String message, String styleSheet) {
        init();
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender"), "+Q Joven"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
            msg.setSubject(subject);

            BodyPart messageBodyPart = new MimeBodyPart();

            if (styleSheet.trim().length() > 0) {
                message = overrideMessage(message, styleSheet);
            }

            messageBodyPart.setContent(message, "text/html; charset=utf-8");

            MimeMultipart mp = new MimeMultipart();
            mp.addBodyPart(messageBodyPart);
            msg.setContent(mp);

            Transport t = session.getTransport("smtp");
            t.connect((String) properties.get("mail.smtp.user"), (String) properties.get("mail.smtp.password"));
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
            return true;
        } catch (MessagingException e) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean sendSms(String phoneNumber, String message) {
        initSms();
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender"), "+Q Joven"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(phoneNumber + "@sms.ice.cr"));
            msg.setSubject("+Q Joven Informa.");
            msg.setText(message);
            Transport t = session.getTransport("smtp");
            t.connect((String) properties.get("mail.smtp.user"), (String) properties.get("mail.smtp.password"));
            t.sendMessage(msg, msg.getAllRecipients());
            t.close();
            return true;
        } catch (MessagingException e) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Email.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private static void initSms() {
        properties.put("mail.smtp.host", "icecom.ice.co.cr");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.mail.sender", "pedrojara50000@ice.co.cr");
        properties.put("mail.smtp.password", "jara5000");
        properties.put("mail.smtp.port", "");
        properties.put("mail.smtp.user", "pedrojar@ice.co.cr");
        properties.put("mail.smtp.auth", "true");
        session = getDefaultInstance(properties);
    }

    public static String overrideMessage(String message, String styleSheet) {
        String value = Utility.readCSSFile(styleSheet);
        message = message.replace("<head>", "<style>" + value + "</style></head>");
        return message;
    }
}
