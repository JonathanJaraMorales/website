/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author jonathan
 */
public class XMLUtils {

    public static String getValue(String link, String tag) {
        String val = "";
        try {
            URL url = new URL(link);
            URLConnection connection = url.openConnection();
            Document doc = parseXML(connection.getInputStream());
            NodeList descNodes = doc.getElementsByTagName(tag);
            val = descNodes.item(0).getTextContent();
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return val;
    }

    public static Document parseXML(InputStream stream) throws Exception {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        } catch (ParserConfigurationException ex) {
            throw ex;
        }
        return doc;
    }

    public static Document getDocument(String direction) {
        Document doc = null;
        try {
            URL url = new URL(direction);
            URLConnection connection = url.openConnection();
            doc = parseXML(connection.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return doc;
    }

    public static Element getRoot(Document document) {
        Element root = null;
        if (document != null) {
            List<Element> list = getChildList(document);
            if (list.size() > 0) {
                root = list.get(0);
            }
        }
        return root;
    }

    public static List<Element> getChildList(Node node) {
        List<Element> list = new ArrayList<Element>();
        NodeList nodeList = node.getChildNodes();
        int size = nodeList.getLength();
        for (int i = 0; i < size; i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                list.add((Element) nodeList.item(i));
            }
        }
        return list;
    }

}
