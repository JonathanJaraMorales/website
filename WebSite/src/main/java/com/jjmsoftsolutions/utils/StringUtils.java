/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jonathan
 */
public class StringUtils {

    public static int getInt(Object value) {
        int toReturn = 0;

        String val = getString(value).replaceAll(",", "");
        try {

            if (val.trim().length() > 0) {
                if (val.contains(".")) {
                    toReturn = (int) Double.parseDouble(val);
                } else {
                    toReturn = Integer.parseInt(val);
                }
            }
        } catch (NumberFormatException e) {
            toReturn = 0;
        }

        return toReturn;
    }

    public static String getString(Object value) {
        return (value == null || value.toString().trim().equals("null")) ? "" : value.toString().trim();
    }

    public static double getDouble(Object value) {
        String val = getString(value);
        val = val.replaceAll(",", "");

        try {
            return "".equals(val) ? 0 : Double.parseDouble(val);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String md5(String string) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] b = md.digest(string.getBytes());

            int size = b.length;
            StringBuffer h = new StringBuffer(size);
            for (int i = 0; i < size; i++) {
                int u = b[i] & 255;
                if (u < 16) {
                    h.append("0" + Integer.toHexString(u));
                } else {
                    h.append(Integer.toHexString(u));
                }
            }
            return h.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(StringUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Attach the specified quantity of characters to the left of the source
     * string until the resulting string lenght equals the maximun specified
     * size.
     *
     * @param source The source string to fill.
     * @param charToFillWith The character to fill the string with.
     * @param max The maximun size of the resulting string.
     * @return The source string with the specified number of characters
     * attached to the left.
     */
    public static String fillLeft(String source, char charToFillWith, int max) {
        StringBuilder sb = new StringBuilder(source == null ? "" : source);
        while (sb.length() < max) {
            sb.insert(0, charToFillWith);
        }
        return sb.toString();
    }
}
