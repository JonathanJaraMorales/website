package com.jjmsoftsolutions.utils;

import com.jjmsoftsolutions.entity.Blog;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import static java.util.logging.Logger.getLogger;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jonathan Jara Morales
 * @version 1.0
 * @since 16/05/2014 Copyright 2014 JJMSoftSolutions
 */
public class Session {

    /**
     * Search the user in seesion
     *
     * @return
     */
    public static Blog getUser() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        return (Blog) session.getAttribute("user");
    }

    /**
     * Search the session object
     *
     * @return
     */
    public static HttpSession getSession() {
        return (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
    }

    /**
     * Redirect to default page when the user is logged
     */
    public static void redirect() {
        try {
            getCurrentInstance().getExternalContext().redirect("faces/Desktop.xhtml");
        } catch (IOException ex) {
            getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getIp() throws Exception {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            String ip = in.readLine();
            return ip;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void logout() {
        HttpSession session = (HttpSession) getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        
    }

}
