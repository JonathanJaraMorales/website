package com.jjmsoftsolutions.utils;

import java.util.Map;
import javax.faces.context.FacesContext;

public class JSFUtil {
    
    public static String getParameterURL(String parameter){
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
        return paramMap.get(parameter);
    }
    
}
