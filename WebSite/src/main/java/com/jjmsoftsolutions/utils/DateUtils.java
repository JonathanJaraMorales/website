package com.jjmsoftsolutions.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    private static final String[] MONTHS_NAMES_COMPLETE = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembte", "Diciembre"};

    public static String getDateInWords(Date date) {
        int day = date.getDay();
        String month = getMonthName(date);
        int year = getYear(date);
        String hour = getHour(date);
        return day + " de " + month + " del " + year + " a " + " las " + hour;
    }

    /**
     * Return the month name
     *
     * @param date
     * @return month name
     */
    public static String getMonthName(Date date) {
        return MONTHS_NAMES_COMPLETE[date.getMonth() + 1];
    }

    public static int getYear(Date date) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        return c.get(Calendar.YEAR);
    }

    public static String getHour(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        String am_pm;
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        if (calendar.get(Calendar.AM_PM) == 0) {
            am_pm = "a.m.";
        } else {
            am_pm = "p.m.";
        }
        return StringUtils.fillLeft("" + hour, '0', 2) + ":" + StringUtils.fillLeft("" + minute, '0', 2) + " " + am_pm;
    }

    public static Date getBeginOfCurrentMonth() {
        Calendar min = Calendar.getInstance();
        min.set(Calendar.DATE, min.getActualMinimum(Calendar.DATE));
        return min.getTime();
    }

    public static Date getEndOfCurrentMonth() {
        Calendar max = Calendar.getInstance();
        max.set(Calendar.DATE, max.getActualMaximum(Calendar.DATE));
        return max.getTime();
    }

}
