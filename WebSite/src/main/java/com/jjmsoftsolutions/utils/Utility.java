package com.jjmsoftsolutions.utils;

import com.jjmsoftsolutions.domain.Tag;
import com.jjmsoftsolutions.model.VideoModel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import static javax.faces.context.FacesContext.getCurrentInstance;
import javax.servlet.http.Part;

/**
 *
 * @author jonathan
 */
public class Utility {

    public static List<Tag> getTag(int cantItems, int totalItems, int pagination) {
        List<Tag> list = new ArrayList<Tag>();
        if (totalItems > 0 && cantItems > 0) {
            int totalButtons = new BigDecimal(totalItems).divide(new BigDecimal(cantItems), RoundingMode.UP).intValue();
            int cont = 0;
            for (int i = 0; i < totalButtons; i++) {
                cont = cont + 3;
                boolean selected = pagination == cont;
                Tag tag = new Tag(cont, selected);
                list.add(tag);
            }
        }
        return list;
    }

    public static void redirect(String url) {
        try {
            getCurrentInstance().getExternalContext().redirect(url);
        } catch (IOException ex) {
            getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getFileName(Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("***** partHeader: " + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim()
                        .replace("\"", "");
            }
        }
        return null;
    }

    public static String readCSSFile(String fileName) {
        StringBuilder out = new StringBuilder();
        try {
            InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("css/".concat(fileName).concat(".css"));
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return out.toString();
    }
}
