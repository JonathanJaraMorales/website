/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import com.jjmsoftsolutions.utils.DateUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "POST_COMMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PostComment.findAll", query = "SELECT p FROM PostComment p"),
    @NamedQuery(name = "PostComment.findByIdPostComment", query = "SELECT p FROM PostComment p WHERE p.idPostComment = :idPostComment"),
    @NamedQuery(name = "PostComment.findByCommentDate", query = "SELECT p FROM PostComment p WHERE p.commentDate = :commentDate")})
public class PostComment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_POST_COMMENT")
    private Integer idPostComment;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "COMMENT")
    private String comment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;
    @JoinColumn(name = "ID_POST", referencedColumnName = "ID_POST")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Post idPost;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;

    public PostComment() {
    }

    public PostComment(Integer idPostComment) {
        this.idPostComment = idPostComment;
    }

    public PostComment(Integer idPostComment, String comment, Date commentDate) {
        this.idPostComment = idPostComment;
        this.comment = comment;
        this.commentDate = commentDate;
    }

    public Integer getIdPostComment() {
        return idPostComment;
    }

    public void setIdPostComment(Integer idPostComment) {
        this.idPostComment = idPostComment;
    }

    public String getComment() {
        return comment;
    }
    
    public String getDateInWords(){        
        return DateUtils.getDateInWords(commentDate);
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Post getIdPost() {
        return idPost;
    }

    public void setIdPost(Post idPost) {
        this.idPost = idPost;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPostComment != null ? idPostComment.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PostComment)) {
            return false;
        }
        PostComment other = (PostComment) object;
        if ((this.idPostComment == null && other.idPostComment != null) || (this.idPostComment != null && !this.idPostComment.equals(other.idPostComment))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.PostComment[ idPostComment=" + idPostComment + " ]";
    }
    
}
