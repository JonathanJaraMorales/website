/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "BLOG_SESSION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BlogSession.findAll", query = "SELECT b FROM BlogSession b"),
    @NamedQuery(name = "BlogSession.findByIdBlogSession", query = "SELECT b FROM BlogSession b WHERE b.idBlogSession = :idBlogSession"),
    @NamedQuery(name = "BlogSession.findByLoginDate", query = "SELECT b FROM BlogSession b WHERE b.loginDate = :loginDate"),
    @NamedQuery(name = "BlogSession.findByIp", query = "SELECT b FROM BlogSession b WHERE b.ip = :ip")})
public class BlogSession implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_BLOG_SESSION")
    private Integer idBlogSession;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LOGIN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loginDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "IP")
    private String ip;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;

    public BlogSession() {
    }

    public BlogSession(Integer idBlogSession) {
        this.idBlogSession = idBlogSession;
    }

    public BlogSession(Integer idBlogSession, Date loginDate, String ip) {
        this.idBlogSession = idBlogSession;
        this.loginDate = loginDate;
        this.ip = ip;
    }

    public Integer getIdBlogSession() {
        return idBlogSession;
    }

    public void setIdBlogSession(Integer idBlogSession) {
        this.idBlogSession = idBlogSession;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBlogSession != null ? idBlogSession.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlogSession)) {
            return false;
        }
        BlogSession other = (BlogSession) object;
        if ((this.idBlogSession == null && other.idBlogSession != null) || (this.idBlogSession != null && !this.idBlogSession.equals(other.idBlogSession))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.BlogSession[ idBlogSession=" + idBlogSession + " ]";
    }
    
}
