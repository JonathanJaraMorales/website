/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "PICTURE_COMMENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PictureComments.findAll", query = "SELECT p FROM PictureComments p"),
    @NamedQuery(name = "PictureComments.findByIdPictureComment", query = "SELECT p FROM PictureComments p WHERE p.idPictureComment = :idPictureComment"),
    @NamedQuery(name = "PictureComments.findByCommentDate", query = "SELECT p FROM PictureComments p WHERE p.commentDate = :commentDate")})
public class PictureComments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PICTURE_COMMENT")
    private Integer idPictureComment;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "COMMENT")
    private String comment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date commentDate;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;
    @JoinColumn(name = "ID_PICTURE", referencedColumnName = "ID_PICTURE")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Picture idPicture;

    public PictureComments() {
    }

    public PictureComments(Integer idPictureComment) {
        this.idPictureComment = idPictureComment;
    }

    public PictureComments(Integer idPictureComment, String comment, Date commentDate) {
        this.idPictureComment = idPictureComment;
        this.comment = comment;
        this.commentDate = commentDate;
    }

    public Integer getIdPictureComment() {
        return idPictureComment;
    }

    public void setIdPictureComment(Integer idPictureComment) {
        this.idPictureComment = idPictureComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    public Picture getIdPicture() {
        return idPicture;
    }

    public void setIdPicture(Picture idPicture) {
        this.idPicture = idPicture;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPictureComment != null ? idPictureComment.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PictureComments)) {
            return false;
        }
        PictureComments other = (PictureComments) object;
        if ((this.idPictureComment == null && other.idPictureComment != null) || (this.idPictureComment != null && !this.idPictureComment.equals(other.idPictureComment))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.PictureComments[ idPictureComment=" + idPictureComment + " ]";
    }
    
}
