/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import com.jjmsoftsolutions.domain.Statistic;
import com.jjmsoftsolutions.model.PollModel;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "POLL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Poll.findAll", query = "SELECT p FROM Poll p"),
    @NamedQuery(name = "Poll.findByIdPoll", query = "SELECT p FROM Poll p WHERE p.idPoll = :idPoll"),
    @NamedQuery(name = "Poll.findByQuestion", query = "SELECT p FROM Poll p WHERE p.question = :question"),
    @NamedQuery(name = "Poll.findByPollDate", query = "SELECT p FROM Poll p WHERE p.pollDate = :pollDate")})
public class Poll implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoll", fetch = FetchType.LAZY)
    private List<PollStats> pollStatsList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_POLL")
    private Integer idPoll;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "QUESTION")
    private String question;
    @Basic(optional = false)
    @NotNull
    @Column(name = "POLL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pollDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoll", fetch = FetchType.LAZY)
    private List<PollOptions> pollOptionsList;

    public Poll() {
    }

    public Poll(Integer idPoll) {
        this.idPoll = idPoll;
    }

    public Poll(Integer idPoll, String question, Date pollDate) {
        this.idPoll = idPoll;
        this.question = question;
        this.pollDate = pollDate;
    }

    public Integer getIdPoll() {
        return idPoll;
    }

    public void setIdPoll(Integer idPoll) {
        this.idPoll = idPoll;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Date getPollDate() {
        return pollDate;
    }

    public void setPollDate(Date pollDate) {
        this.pollDate = pollDate;
    }

    @XmlTransient
    public List<PollOptions> getPollOptionsList() {
        return pollOptionsList;
    }

    public void setPollOptionsList(List<PollOptions> pollOptionsList) {
        this.pollOptionsList = pollOptionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPoll != null ? idPoll.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Poll)) {
            return false;
        }
        Poll other = (Poll) object;
        if ((this.idPoll == null && other.idPoll != null) || (this.idPoll != null && !this.idPoll.equals(other.idPoll))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Poll[ idPoll=" + idPoll + " ]";
    }
    
    public List<Statistic> getPercentage(){
        return PollModel.getStatisTic(idPoll);
    }
    
    public PollStats getUserVote(){
        return PollModel.userVote(idPoll);
    }

    @XmlTransient
    public List<PollStats> getPollStatsList() {
        return pollStatsList;
    }

    public void setPollStatsList(List<PollStats> pollStatsList) {
        this.pollStatsList = pollStatsList;
    }
    
}
