/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "POLL_STATS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PollStats.findAll", query = "SELECT p FROM PollStats p"),
    @NamedQuery(name = "PollStats.findByIdPollStats", query = "SELECT p FROM PollStats p WHERE p.idPollStats = :idPollStats")})
public class PollStats implements Serializable {
    @JoinColumn(name = "ID_POLL", referencedColumnName = "ID_POLL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Poll idPoll;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_POLL_STATS")
    private Integer idPollStats;
    @JoinColumn(name = "ID_POLL_OPTION", referencedColumnName = "ID_POLL_OPTIONS")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PollOptions idPollOption;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;

    public PollStats() {
    }

    public PollStats(Integer idPollStats) {
        this.idPollStats = idPollStats;
    }

    public Integer getIdPollStats() {
        return idPollStats;
    }

    public void setIdPollStats(Integer idPollStats) {
        this.idPollStats = idPollStats;
    }

    public PollOptions getIdPollOption() {
        return idPollOption;
    }

    public void setIdPollOption(PollOptions idPollOption) {
        this.idPollOption = idPollOption;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPollStats != null ? idPollStats.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PollStats)) {
            return false;
        }
        PollStats other = (PollStats) object;
        if ((this.idPollStats == null && other.idPollStats != null) || (this.idPollStats != null && !this.idPollStats.equals(other.idPollStats))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.PollStats[ idPollStats=" + idPollStats + " ]";
    }

    public Poll getIdPoll() {
        return idPoll;
    }

    public void setIdPoll(Poll idPoll) {
        this.idPoll = idPoll;
    }
    
}
