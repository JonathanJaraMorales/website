/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Your Name <your.name at your.org>
 */
@Entity
@Table(name = "BLOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Blog.findAll", query = "SELECT b FROM Blog b"),
    @NamedQuery(name = "Blog.findByEmail", query = "SELECT b FROM Blog b WHERE b.email = :email"),
    @NamedQuery(name = "Blog.findByUsername", query = "SELECT b FROM Blog b WHERE b.username = :username"),
    @NamedQuery(name = "Blog.findByPassword", query = "SELECT b FROM Blog b WHERE b.password = :password"),
    @NamedQuery(name = "Blog.findByName", query = "SELECT b FROM Blog b WHERE b.name = :name"),
    @NamedQuery(name = "Blog.findByFirstName", query = "SELECT b FROM Blog b WHERE b.firstName = :firstName"),
    @NamedQuery(name = "Blog.findByLastName", query = "SELECT b FROM Blog b WHERE b.lastName = :lastName"),
    @NamedQuery(name = "Blog.findBySecondLastName", query = "SELECT b FROM Blog b WHERE b.secondLastName = :secondLastName"),
    @NamedQuery(name = "Blog.findByPhone", query = "SELECT b FROM Blog b WHERE b.phone = :phone"),
    @NamedQuery(name = "Blog.findByCellphone", query = "SELECT b FROM Blog b WHERE b.cellphone = :cellphone"),
    @NamedQuery(name = "Blog.findByGender", query = "SELECT b FROM Blog b WHERE b.gender = :gender"),
    @NamedQuery(name = "Blog.findByBirthDate", query = "SELECT b FROM Blog b WHERE b.birthDate = :birthDate"),
    @NamedQuery(name = "Blog.findByActivate", query = "SELECT b FROM Blog b WHERE b.activate = :activate"),
    @NamedQuery(name = "Blog.findByLoginDate", query = "SELECT b FROM Blog b WHERE b.loginDate = :loginDate"),
    @NamedQuery(name = "Blog.findByLastLoginDate", query = "SELECT b FROM Blog b WHERE b.lastLoginDate = :lastLoginDate"),
    @NamedQuery(name = "Blog.findByLastIpAccess", query = "SELECT b FROM Blog b WHERE b.lastIpAccess = :lastIpAccess"),
    @NamedQuery(name = "Blog.findByPicture", query = "SELECT b FROM Blog b WHERE b.picture = :picture")})
public class Blog implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "email", fetch = FetchType.LAZY)
    private List<BlogSecurity> blogSecurityList;
    private static final long serialVersionUID = 1L;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "PASSWORD")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "NAME")
    private String name;
    @Size(max = 45)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 45)
    @Column(name = "LAST_NAME")
    private String lastName;
    @Size(max = 45)
    @Column(name = "SECOND_LAST_NAME")
    private String secondLastName;
    @Column(name = "PHONE")
    private Integer phone;
    @Column(name = "CELLPHONE")
    private Integer cellphone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GENDER")
    private Character gender;
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Column(name = "ACTIVATE")
    private Character activate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LOGIN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loginDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LAST_LOGIN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "LAST_IP_ACCESS")
    private String lastIpAccess;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "PICTURE")
    private String picture;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "email", fetch = FetchType.LAZY)
    private List<BlogProperties> blogPropertiesList;

    public Blog() {
    }

    public Blog(String email) {
        this.email = email;
    }

    public Blog(String email, String username, String password, String name, Character gender, Date loginDate, Date lastLoginDate, String lastIpAccess, String picture) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.name = name;
        this.gender = gender;
        this.loginDate = loginDate;
        this.lastLoginDate = lastLoginDate;
        this.lastIpAccess = lastIpAccess;
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Integer getCellphone() {
        return cellphone;
    }

    public void setCellphone(Integer cellphone) {
        this.cellphone = cellphone;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Character getActivate() {
        return activate;
    }

    public void setActivate(Character activate) {
        this.activate = activate;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLastIpAccess() {
        return lastIpAccess;
    }

    public void setLastIpAccess(String lastIpAccess) {
        this.lastIpAccess = lastIpAccess;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @XmlTransient
    public List<BlogProperties> getBlogPropertiesList() {
        return blogPropertiesList;
    }

    public void setBlogPropertiesList(List<BlogProperties> blogPropertiesList) {
        this.blogPropertiesList = blogPropertiesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Blog)) {
            return false;
        }
        Blog other = (Blog) object;
        if ((this.email == null && other.email != null) || (this.email != null && !this.email.equals(other.email))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Blog[ email=" + email + " ]";
    }

    @XmlTransient
    public List<BlogSecurity> getBlogSecurityList() {
        return blogSecurityList;
    }

    public void setBlogSecurityList(List<BlogSecurity> blogSecurityList) {
        this.blogSecurityList = blogSecurityList;
    }

    public BlogSecurity getSecurity() {
        if (blogSecurityList != null && blogSecurityList.size() > 0) {
            return blogSecurityList.get(0);
        }
        return null;
    }

    public BlogProperties getProperties() {
        if (blogPropertiesList != null && blogPropertiesList.size() > 0) {
            return blogPropertiesList.get(0);
        }
        return null;
    }

}
