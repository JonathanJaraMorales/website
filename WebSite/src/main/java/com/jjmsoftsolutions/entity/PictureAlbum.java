/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "PICTURE_ALBUM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PictureAlbum.findAll", query = "SELECT p FROM PictureAlbum p"),
    @NamedQuery(name = "PictureAlbum.findByIdPictureAlbum", query = "SELECT p FROM PictureAlbum p WHERE p.idPictureAlbum = :idPictureAlbum"),
    @NamedQuery(name = "PictureAlbum.findByCategory", query = "SELECT p FROM PictureAlbum p WHERE p.category = :category")})
public class PictureAlbum implements Serializable {
    @Size(max = 45)
    @Column(name = "TITLE")
    private String title;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PICTURE")
    private String picture;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PICTURE_ALBUM")
    private Integer idPictureAlbum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "CATEGORY")
    private String category;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPictureAlbum", fetch = FetchType.LAZY)
    private List<Picture> pictureList;

    public PictureAlbum() {
    }

    public PictureAlbum(Integer idPictureAlbum) {
        this.idPictureAlbum = idPictureAlbum;
    }

    public PictureAlbum(Integer idPictureAlbum, String category) {
        this.idPictureAlbum = idPictureAlbum;
        this.category = category;
    }

    public Integer getIdPictureAlbum() {
        return idPictureAlbum;
    }

    public void setIdPictureAlbum(Integer idPictureAlbum) {
        this.idPictureAlbum = idPictureAlbum;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @XmlTransient
    public List<Picture> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<Picture> pictureList) {
        this.pictureList = pictureList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPictureAlbum != null ? idPictureAlbum.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PictureAlbum)) {
            return false;
        }
        PictureAlbum other = (PictureAlbum) object;
        if ((this.idPictureAlbum == null && other.idPictureAlbum != null) || (this.idPictureAlbum != null && !this.idPictureAlbum.equals(other.idPictureAlbum))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.PictureAlbum[ idPictureAlbum=" + idPictureAlbum + " ]";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    
}
