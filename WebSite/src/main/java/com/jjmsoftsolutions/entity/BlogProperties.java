/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Your Name <your.name at your.org>
 */
@Entity
@Table(name = "BLOG_PROPERTIES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BlogProperties.findAll", query = "SELECT b FROM BlogProperties b"),
    @NamedQuery(name = "BlogProperties.findByIdBlogProperties", query = "SELECT b FROM BlogProperties b WHERE b.idBlogProperties = :idBlogProperties"),
    @NamedQuery(name = "BlogProperties.findBySendEmailSermon", query = "SELECT b FROM BlogProperties b WHERE b.sendEmailSermon = :sendEmailSermon"),
    @NamedQuery(name = "BlogProperties.findBySendEmailEvent", query = "SELECT b FROM BlogProperties b WHERE b.sendEmailEvent = :sendEmailEvent"),
    @NamedQuery(name = "BlogProperties.findBySendEmailPost", query = "SELECT b FROM BlogProperties b WHERE b.sendEmailPost = :sendEmailPost"),
    @NamedQuery(name = "BlogProperties.findBySendEmailCuriosity", query = "SELECT b FROM BlogProperties b WHERE b.sendEmailCuriosity = :sendEmailCuriosity"),
    @NamedQuery(name = "BlogProperties.findBySendEmailPhotos", query = "SELECT b FROM BlogProperties b WHERE b.sendEmailPhotos = :sendEmailPhotos")})
public class BlogProperties implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_BLOG_PROPERTIES")
    private Integer idBlogProperties;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_EMAIL_SERMON")
    private Character sendEmailSermon;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_EMAIL_EVENT")
    private Character sendEmailEvent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_EMAIL_POST")
    private Character sendEmailPost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_EMAIL_CURIOSITY")
    private Character sendEmailCuriosity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEND_EMAIL_PHOTOS")
    private Character sendEmailPhotos;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;

    public BlogProperties() {
    }

    public BlogProperties(Integer idBlogProperties) {
        this.idBlogProperties = idBlogProperties;
    }

    public BlogProperties(Integer idBlogProperties, Character sendEmailSermon, Character sendEmailEvent, Character sendEmailPost, Character sendEmailCuriosity, Character sendEmailPhotos) {
        this.idBlogProperties = idBlogProperties;
        this.sendEmailSermon = sendEmailSermon;
        this.sendEmailEvent = sendEmailEvent;
        this.sendEmailPost = sendEmailPost;
        this.sendEmailCuriosity = sendEmailCuriosity;
        this.sendEmailPhotos = sendEmailPhotos;
    }

    public Integer getIdBlogProperties() {
        return idBlogProperties;
    }

    public void setIdBlogProperties(Integer idBlogProperties) {
        this.idBlogProperties = idBlogProperties;
    }

    public Character getSendEmailSermon() {
        return sendEmailSermon;
    }

    public void setSendEmailSermon(Character sendEmailSermon) {
        this.sendEmailSermon = sendEmailSermon;
    }

    public Character getSendEmailEvent() {
        return sendEmailEvent;
    }

    public void setSendEmailEvent(Character sendEmailEvent) {
        this.sendEmailEvent = sendEmailEvent;
    }

    public Character getSendEmailPost() {
        return sendEmailPost;
    }

    public void setSendEmailPost(Character sendEmailPost) {
        this.sendEmailPost = sendEmailPost;
    }

    public Character getSendEmailCuriosity() {
        return sendEmailCuriosity;
    }

    public void setSendEmailCuriosity(Character sendEmailCuriosity) {
        this.sendEmailCuriosity = sendEmailCuriosity;
    }

    public Character getSendEmailPhotos() {
        return sendEmailPhotos;
    }

    public void setSendEmailPhotos(Character sendEmailPhotos) {
        this.sendEmailPhotos = sendEmailPhotos;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBlogProperties != null ? idBlogProperties.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlogProperties)) {
            return false;
        }
        BlogProperties other = (BlogProperties) object;
        if ((this.idBlogProperties == null && other.idBlogProperties != null) || (this.idBlogProperties != null && !this.idBlogProperties.equals(other.idBlogProperties))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.BlogProperties[ idBlogProperties=" + idBlogProperties + " ]";
    }
    
}
