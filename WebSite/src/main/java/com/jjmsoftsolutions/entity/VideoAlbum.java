/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "VIDEO_ALBUM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VideoAlbum.findAll", query = "SELECT v FROM VideoAlbum v"),
    @NamedQuery(name = "VideoAlbum.findByIdVideoAlbum", query = "SELECT v FROM VideoAlbum v WHERE v.idVideoAlbum = :idVideoAlbum"),
    @NamedQuery(name = "VideoAlbum.findByCategory", query = "SELECT v FROM VideoAlbum v WHERE v.category = :category")})
public class VideoAlbum implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_VIDEO_ALBUM")
    private Integer idVideoAlbum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "CATEGORY")
    private String category;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVideoAlbum", fetch = FetchType.LAZY)
    private List<Video> videoList;

    public VideoAlbum() {
    }

    public VideoAlbum(Integer idVideoAlbum) {
        this.idVideoAlbum = idVideoAlbum;
    }

    public VideoAlbum(Integer idVideoAlbum, String category) {
        this.idVideoAlbum = idVideoAlbum;
        this.category = category;
    }

    public Integer getIdVideoAlbum() {
        return idVideoAlbum;
    }

    public void setIdVideoAlbum(Integer idVideoAlbum) {
        this.idVideoAlbum = idVideoAlbum;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @XmlTransient
    public List<Video> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<Video> videoList) {
        this.videoList = videoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVideoAlbum != null ? idVideoAlbum.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VideoAlbum)) {
            return false;
        }
        VideoAlbum other = (VideoAlbum) object;
        if ((this.idVideoAlbum == null && other.idVideoAlbum != null) || (this.idVideoAlbum != null && !this.idVideoAlbum.equals(other.idVideoAlbum))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.VideoAlbum[ idVideoAlbum=" + idVideoAlbum + " ]";
    }
    
}
