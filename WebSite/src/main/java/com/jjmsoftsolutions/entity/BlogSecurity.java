/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "BLOG_SECURITY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BlogSecurity.findAll", query = "SELECT b FROM BlogSecurity b"),
    @NamedQuery(name = "BlogSecurity.findByIdBlogSecurity", query = "SELECT b FROM BlogSecurity b WHERE b.idBlogSecurity = :idBlogSecurity"),
    @NamedQuery(name = "BlogSecurity.findByShowEmail", query = "SELECT b FROM BlogSecurity b WHERE b.showEmail = :showEmail"),
    @NamedQuery(name = "BlogSecurity.findByShowPhone", query = "SELECT b FROM BlogSecurity b WHERE b.showPhone = :showPhone"),
    @NamedQuery(name = "BlogSecurity.findByShowProfile", query = "SELECT b FROM BlogSecurity b WHERE b.showProfile = :showProfile"),
    @NamedQuery(name = "BlogSecurity.findByShowActivity", query = "SELECT b FROM BlogSecurity b WHERE b.showActivity = :showActivity"),
    @NamedQuery(name = "BlogSecurity.findByShowLastConnection", query = "SELECT b FROM BlogSecurity b WHERE b.showLastConnection = :showLastConnection"),
    @NamedQuery(name = "BlogSecurity.findByShowName", query = "SELECT b FROM BlogSecurity b WHERE b.showName = :showName"),
    @NamedQuery(name = "BlogSecurity.findByShowFirstName", query = "SELECT b FROM BlogSecurity b WHERE b.showFirstName = :showFirstName"),
    @NamedQuery(name = "BlogSecurity.findByShowLastName", query = "SELECT b FROM BlogSecurity b WHERE b.showLastName = :showLastName"),
    @NamedQuery(name = "BlogSecurity.findByShowSecondLastName", query = "SELECT b FROM BlogSecurity b WHERE b.showSecondLastName = :showSecondLastName"),
    @NamedQuery(name = "BlogSecurity.findByShowGender", query = "SELECT b FROM BlogSecurity b WHERE b.showGender = :showGender"),
    @NamedQuery(name = "BlogSecurity.findByShowBirthDate", query = "SELECT b FROM BlogSecurity b WHERE b.showBirthDate = :showBirthDate"),
    @NamedQuery(name = "BlogSecurity.findByShowStatusConecction", query = "SELECT b FROM BlogSecurity b WHERE b.showStatusConecction = :showStatusConecction")})
public class BlogSecurity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_BLOG_SECURITY")
    private Integer idBlogSecurity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_EMAIL")
    private Character showEmail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_PHONE")
    private Character showPhone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_PROFILE")
    private Character showProfile;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_ACTIVITY")
    private Character showActivity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_LAST_CONNECTION")
    private Character showLastConnection;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_NAME")
    private Character showName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_FIRST_NAME")
    private Character showFirstName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_LAST_NAME")
    private Character showLastName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_SECOND_LAST_NAME")
    private Character showSecondLastName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_GENDER")
    private Character showGender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_BIRTH_DATE")
    private Character showBirthDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SHOW_STATUS_CONECCTION")
    private Character showStatusConecction;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;

    public BlogSecurity() {
    }

    public BlogSecurity(Integer idBlogSecurity) {
        this.idBlogSecurity = idBlogSecurity;
    }

    public BlogSecurity(Integer idBlogSecurity, Character showEmail, Character showPhone, Character showProfile, Character showActivity, Character showLastConnection, Character showName, Character showFirstName, Character showLastName, Character showSecondLastName, Character showGender, Character showBirthDate, Character showStatusConecction) {
        this.idBlogSecurity = idBlogSecurity;
        this.showEmail = showEmail;
        this.showPhone = showPhone;
        this.showProfile = showProfile;
        this.showActivity = showActivity;
        this.showLastConnection = showLastConnection;
        this.showName = showName;
        this.showFirstName = showFirstName;
        this.showLastName = showLastName;
        this.showSecondLastName = showSecondLastName;
        this.showGender = showGender;
        this.showBirthDate = showBirthDate;
        this.showStatusConecction = showStatusConecction;
    }

    public Integer getIdBlogSecurity() {
        return idBlogSecurity;
    }

    public void setIdBlogSecurity(Integer idBlogSecurity) {
        this.idBlogSecurity = idBlogSecurity;
    }

    public Character getShowEmail() {
        return showEmail;
    }

    public void setShowEmail(Character showEmail) {
        this.showEmail = showEmail;
    }

    public Character getShowPhone() {
        return showPhone;
    }

    public void setShowPhone(Character showPhone) {
        this.showPhone = showPhone;
    }

    public Character getShowProfile() {
        return showProfile;
    }

    public void setShowProfile(Character showProfile) {
        this.showProfile = showProfile;
    }

    public Character getShowActivity() {
        return showActivity;
    }

    public void setShowActivity(Character showActivity) {
        this.showActivity = showActivity;
    }

    public Character getShowLastConnection() {
        return showLastConnection;
    }

    public void setShowLastConnection(Character showLastConnection) {
        this.showLastConnection = showLastConnection;
    }

    public Character getShowName() {
        return showName;
    }

    public void setShowName(Character showName) {
        this.showName = showName;
    }

    public Character getShowFirstName() {
        return showFirstName;
    }

    public void setShowFirstName(Character showFirstName) {
        this.showFirstName = showFirstName;
    }

    public Character getShowLastName() {
        return showLastName;
    }

    public void setShowLastName(Character showLastName) {
        this.showLastName = showLastName;
    }

    public Character getShowSecondLastName() {
        return showSecondLastName;
    }

    public void setShowSecondLastName(Character showSecondLastName) {
        this.showSecondLastName = showSecondLastName;
    }

    public Character getShowGender() {
        return showGender;
    }

    public void setShowGender(Character showGender) {
        this.showGender = showGender;
    }

    public Character getShowBirthDate() {
        return showBirthDate;
    }

    public void setShowBirthDate(Character showBirthDate) {
        this.showBirthDate = showBirthDate;
    }

    public Character getShowStatusConecction() {
        return showStatusConecction;
    }

    public void setShowStatusConecction(Character showStatusConecction) {
        this.showStatusConecction = showStatusConecction;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBlogSecurity != null ? idBlogSecurity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlogSecurity)) {
            return false;
        }
        BlogSecurity other = (BlogSecurity) object;
        if ((this.idBlogSecurity == null && other.idBlogSecurity != null) || (this.idBlogSecurity != null && !this.idBlogSecurity.equals(other.idBlogSecurity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.BlogSecurity[ idBlogSecurity=" + idBlogSecurity + " ]";
    }
    
}
