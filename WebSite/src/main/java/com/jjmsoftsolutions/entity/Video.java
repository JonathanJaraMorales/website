/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "VIDEO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Video.findAll", query = "SELECT v FROM Video v"),
    @NamedQuery(name = "Video.findByIdVideo", query = "SELECT v FROM Video v WHERE v.idVideo = :idVideo"),
    @NamedQuery(name = "Video.findByVideoDate", query = "SELECT v FROM Video v WHERE v.videoDate = :videoDate")})
public class Video implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_VIDEO")
    private Integer idVideo;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "URL")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "TITLE")
    private String title;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VIDEO_DATE")
    @Temporal(TemporalType.DATE)
    private Date videoDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVideo", fetch = FetchType.LAZY)
    private List<VideoComments> videoCommentsList;
    @JoinColumn(name = "ID_EXPONENT", referencedColumnName = "ID_EXPONENT")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Exponent idExponent;
    @JoinColumn(name = "ID_VIDEO_ALBUM", referencedColumnName = "ID_VIDEO_ALBUM")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private VideoAlbum idVideoAlbum;

    public Video() {
    }

    public Video(Integer idVideo) {
        this.idVideo = idVideo;
    }

    public Video(Integer idVideo, String url, String title, Date videoDate) {
        this.idVideo = idVideo;
        this.url = url;
        this.title = title;
        this.videoDate = videoDate;
    }

    public Integer getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(Integer idVideo) {
        this.idVideo = idVideo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getVideoDate() {
        return videoDate;
    }

    public void setVideoDate(Date videoDate) {
        this.videoDate = videoDate;
    }

    @XmlTransient
    public List<VideoComments> getVideoCommentsList() {
        return videoCommentsList;
    }

    public void setVideoCommentsList(List<VideoComments> videoCommentsList) {
        this.videoCommentsList = videoCommentsList;
    }

    public Exponent getIdExponent() {
        return idExponent;
    }

    public void setIdExponent(Exponent idExponent) {
        this.idExponent = idExponent;
    }

    public VideoAlbum getIdVideoAlbum() {
        return idVideoAlbum;
    }

    public void setIdVideoAlbum(VideoAlbum idVideoAlbum) {
        this.idVideoAlbum = idVideoAlbum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVideo != null ? idVideo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Video)) {
            return false;
        }
        Video other = (Video) object;
        if ((this.idVideo == null && other.idVideo != null) || (this.idVideo != null && !this.idVideo.equals(other.idVideo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Video[ idVideo=" + idVideo + " ]";
    }

    public String getImage() {
        return "http://img.youtube.com/vi/" + getCode() + "/0.jpg";
    }

    public String getURLYoutube() {
        return "http://www.youtube.com/embed/" + getCode() + "?wmode=transparent";
    }

    public String getCode() {
        String img;
        if (this.url.contains("https")) {
            img = this.url.substring(32);
        } else {
            img = this.url.substring(31);
        }
        return img;
    }

}
