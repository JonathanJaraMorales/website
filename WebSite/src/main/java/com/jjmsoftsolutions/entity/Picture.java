/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import com.jjmsoftsolutions.utils.Constants;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "PICTURE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Picture.findAll", query = "SELECT p FROM Picture p"),
    @NamedQuery(name = "Picture.findByIdPicture", query = "SELECT p FROM Picture p WHERE p.idPicture = :idPicture"),
    @NamedQuery(name = "Picture.findByPictureDate", query = "SELECT p FROM Picture p WHERE p.pictureDate = :pictureDate")})
public class Picture implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PICTURE")
    private Integer idPicture;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "TITLE")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "URL")
    private String url;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PICTURE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pictureDate;
    @JoinColumn(name = "ID_PICTURE_ALBUM", referencedColumnName = "ID_PICTURE_ALBUM")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PictureAlbum idPictureAlbum;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPicture", fetch = FetchType.LAZY)
    private List<PictureComments> pictureCommentsList;

    public Picture() {
    }

    public Picture(Integer idPicture) {
        this.idPicture = idPicture;
    }

    public Picture(Integer idPicture, String title, String url, Date pictureDate) {
        this.idPicture = idPicture;
        this.title = title;
        this.url = url;
        this.pictureDate = pictureDate;
    }

    public Integer getIdPicture() {
        return idPicture;
    }

    public void setIdPicture(Integer idPicture) {
        this.idPicture = idPicture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPictureDate() {
        return pictureDate;
    }

    public void setPictureDate(Date pictureDate) {
        this.pictureDate = pictureDate;
    }

    public PictureAlbum getIdPictureAlbum() {
        return idPictureAlbum;
    }

    public void setIdPictureAlbum(PictureAlbum idPictureAlbum) {
        this.idPictureAlbum = idPictureAlbum;
    }

    @XmlTransient
    public List<PictureComments> getPictureCommentsList() {
        return pictureCommentsList;
    }

    public void setPictureCommentsList(List<PictureComments> pictureCommentsList) {
        this.pictureCommentsList = pictureCommentsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPicture != null ? idPicture.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Picture)) {
            return false;
        }
        Picture other = (Picture) object;
        if ((this.idPicture == null && other.idPicture != null) || (this.idPicture != null && !this.idPicture.equals(other.idPicture))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Picture[ idPicture=" + idPicture + " ]";
    }    
}
