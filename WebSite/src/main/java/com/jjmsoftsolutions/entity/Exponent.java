/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "EXPONENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Exponent.findAll", query = "SELECT e FROM Exponent e"),
    @NamedQuery(name = "Exponent.findByIdExponent", query = "SELECT e FROM Exponent e WHERE e.idExponent = :idExponent"),
    @NamedQuery(name = "Exponent.findByFirstName", query = "SELECT e FROM Exponent e WHERE e.firstName = :firstName"),
    @NamedQuery(name = "Exponent.findByLastname", query = "SELECT e FROM Exponent e WHERE e.lastname = :lastname"),
    @NamedQuery(name = "Exponent.findBySecondLastname", query = "SELECT e FROM Exponent e WHERE e.secondLastname = :secondLastname")})
public class Exponent implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_EXPONENT")
    private Integer idExponent;
    @Size(max = 15)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 15)
    @Column(name = "LASTNAME")
    private String lastname;
    @Size(max = 15)
    @Column(name = "SECOND_LASTNAME")
    private String secondLastname;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idExponent", fetch = FetchType.LAZY)
    private List<Video> videoList;

    public Exponent() {
    }

    public Exponent(Integer idExponent) {
        this.idExponent = idExponent;
    }

    public Integer getIdExponent() {
        return idExponent;
    }

    public void setIdExponent(Integer idExponent) {
        this.idExponent = idExponent;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSecondLastname() {
        return secondLastname;
    }

    public void setSecondLastname(String secondLastname) {
        this.secondLastname = secondLastname;
    }

    @XmlTransient
    public List<Video> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<Video> videoList) {
        this.videoList = videoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExponent != null ? idExponent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exponent)) {
            return false;
        }
        Exponent other = (Exponent) object;
        if ((this.idExponent == null && other.idExponent != null) || (this.idExponent != null && !this.idExponent.equals(other.idExponent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Exponent[ idExponent=" + idExponent + " ]";
    }
    
}
