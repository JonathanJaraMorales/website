/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "POLL_OPTIONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PollOptions.findAll", query = "SELECT p FROM PollOptions p"),
    @NamedQuery(name = "PollOptions.findByIdPollOptions", query = "SELECT p FROM PollOptions p WHERE p.idPollOptions = :idPollOptions"),
    @NamedQuery(name = "PollOptions.findByDescription", query = "SELECT p FROM PollOptions p WHERE p.description = :description")})
public class PollOptions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_POLL_OPTIONS")
    private Integer idPollOptions;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "DESCRIPTION")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPollOption", fetch = FetchType.LAZY)
    private List<PollStats> pollStatsList;
    @JoinColumn(name = "ID_POLL", referencedColumnName = "ID_POLL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Poll idPoll;

    public PollOptions() {
    }

    public PollOptions(Integer idPollOptions) {
        this.idPollOptions = idPollOptions;
    }

    public PollOptions(Integer idPollOptions, String description) {
        this.idPollOptions = idPollOptions;
        this.description = description;
    }

    public Integer getIdPollOptions() {
        return idPollOptions;
    }

    public void setIdPollOptions(Integer idPollOptions) {
        this.idPollOptions = idPollOptions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<PollStats> getPollStatsList() {
        return pollStatsList;
    }
    
    public PollStats getPollStat(){
        return idPoll.getPollStatsList().get(0);
    }

    public void setPollStatsList(List<PollStats> pollStatsList) {
        this.pollStatsList = pollStatsList;
    }

    public Poll getIdPoll() {
        return idPoll;
    }

    public void setIdPoll(Poll idPoll) {
        this.idPoll = idPoll;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPollOptions != null ? idPollOptions.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PollOptions)) {
            return false;
        }
        PollOptions other = (PollOptions) object;
        if ((this.idPollOptions == null && other.idPollOptions != null) || (this.idPollOptions != null && !this.idPollOptions.equals(other.idPollOptions))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.PollOptions[ idPollOptions=" + idPollOptions + " ]";
    }
    
}
