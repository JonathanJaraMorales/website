/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "POST_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PostCategory.findAll", query = "SELECT p FROM PostCategory p"),
    @NamedQuery(name = "PostCategory.findByIdPostCategory", query = "SELECT p FROM PostCategory p WHERE p.idPostCategory = :idPostCategory"),
    @NamedQuery(name = "PostCategory.findByName", query = "SELECT p FROM PostCategory p WHERE p.name = :name")})
public class PostCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_POST_CATEGORY")
    private Integer idPostCategory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAME")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPostCategory", fetch = FetchType.LAZY)
    private List<Post> postList;

    public PostCategory() {
    }

    public PostCategory(Integer idPostCategory) {
        this.idPostCategory = idPostCategory;
    }

    public PostCategory(Integer idPostCategory, String name) {
        this.idPostCategory = idPostCategory;
        this.name = name;
    }

    public Integer getIdPostCategory() {
        return idPostCategory;
    }

    public void setIdPostCategory(Integer idPostCategory) {
        this.idPostCategory = idPostCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPostCategory != null ? idPostCategory.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PostCategory)) {
            return false;
        }
        PostCategory other = (PostCategory) object;
        if ((this.idPostCategory == null && other.idPostCategory != null) || (this.idPostCategory != null && !this.idPostCategory.equals(other.idPostCategory))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.PostCategory[ idPostCategory=" + idPostCategory + " ]";
    }
    
}
