/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "PROPERTIES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Properties.findAll", query = "SELECT p FROM Properties p"),
    @NamedQuery(name = "Properties.findByIdProperties", query = "SELECT p FROM Properties p WHERE p.idProperties = :idProperties"),
    @NamedQuery(name = "Properties.findBySlinderPagination", query = "SELECT p FROM Properties p WHERE p.slinderPagination = :slinderPagination"),
    @NamedQuery(name = "Properties.findBySermonPagination", query = "SELECT p FROM Properties p WHERE p.sermonPagination = :sermonPagination"),
    @NamedQuery(name = "Properties.findByEventPagination", query = "SELECT p FROM Properties p WHERE p.eventPagination = :eventPagination")})
public class Properties implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_PROPERTIES")
    private Integer idProperties;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SLINDER_PAGINATION")
    private int slinderPagination;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERMON_PAGINATION")
    private int sermonPagination;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENT_PAGINATION")
    private int eventPagination;

    public Properties() {
    }

    public Properties(Integer idProperties) {
        this.idProperties = idProperties;
    }

    public Properties(Integer idProperties, int slinderPagination, int sermonPagination, int eventPagination) {
        this.idProperties = idProperties;
        this.slinderPagination = slinderPagination;
        this.sermonPagination = sermonPagination;
        this.eventPagination = eventPagination;
    }

    public Integer getIdProperties() {
        return idProperties;
    }

    public void setIdProperties(Integer idProperties) {
        this.idProperties = idProperties;
    }

    public int getSlinderPagination() {
        return slinderPagination;
    }

    public void setSlinderPagination(int slinderPagination) {
        this.slinderPagination = slinderPagination;
    }

    public int getSermonPagination() {
        return sermonPagination;
    }

    public void setSermonPagination(int sermonPagination) {
        this.sermonPagination = sermonPagination;
    }

    public int getEventPagination() {
        return eventPagination;
    }

    public void setEventPagination(int eventPagination) {
        this.eventPagination = eventPagination;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProperties != null ? idProperties.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Properties)) {
            return false;
        }
        Properties other = (Properties) object;
        if ((this.idProperties == null && other.idProperties != null) || (this.idProperties != null && !this.idProperties.equals(other.idProperties))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.Properties[ idProperties=" + idProperties + " ]";
    }
    
}
