/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jjmsoftsolutions.entity;

import com.jjmsoftsolutions.utils.DateUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luis
 */
@Entity
@Table(name = "VIDEO_COMMENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VideoComments.findAll", query = "SELECT v FROM VideoComments v"),
    @NamedQuery(name = "VideoComments.findByIdVideoComment", query = "SELECT v FROM VideoComments v WHERE v.idVideoComment = :idVideoComment"),
    @NamedQuery(name = "VideoComments.findByCommentDate", query = "SELECT v FROM VideoComments v WHERE v.commentDate = :commentDate")})
public class VideoComments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_VIDEO_COMMENT")
    private Integer idVideoComment;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "COMMENT")
    private String comment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;
    @JoinColumn(name = "EMAIL", referencedColumnName = "EMAIL")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Blog email;
    @JoinColumn(name = "ID_VIDEO", referencedColumnName = "ID_VIDEO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Video idVideo;

    public VideoComments() {
    }

    public VideoComments(Integer idVideoComment) {
        this.idVideoComment = idVideoComment;
    }

    public VideoComments(Integer idVideoComment, String comment, Date commentDate) {
        this.idVideoComment = idVideoComment;
        this.comment = comment;
        this.commentDate = commentDate;
    }

    public Integer getIdVideoComment() {
        return idVideoComment;
    }

    public void setIdVideoComment(Integer idVideoComment) {
        this.idVideoComment = idVideoComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }
    
    public String getDateInWords(){        
        return DateUtils.getDateInWords(commentDate);
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Blog getEmail() {
        return email;
    }

    public void setEmail(Blog email) {
        this.email = email;
    }

    public Video getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(Video idVideo) {
        this.idVideo = idVideo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVideoComment != null ? idVideoComment.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VideoComments)) {
            return false;
        }
        VideoComments other = (VideoComments) object;
        if ((this.idVideoComment == null && other.idVideoComment != null) || (this.idVideoComment != null && !this.idVideoComment.equals(other.idVideoComment))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jjmsoftsolutions.entity.VideoComments[ idVideoComment=" + idVideoComment + " ]";
    }
    
}
